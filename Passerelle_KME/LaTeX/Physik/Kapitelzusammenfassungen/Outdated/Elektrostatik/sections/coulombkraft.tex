Das Coulomb-Gesetz beschreibt die Kraft zwischen zwei Punktladungen\footnote{Eine elektrische Ladung ohne räumliche Ausdehnung. Eine Vereinfachung zur
einfacheren Beschreibung; Analogie zur Mechanik: Massepunkt.}. Die elektrische Ladung eines Körpers ist wie die Masse eine
grundlegende Eigenschaft von Materie, die in der Einheit \textbf{Coulomb} gemessen wird. 
Bringt man zwei geladene Körper nah genug zusammen, so wirkt eine Kraft zwischen 
ihnen. Gleichartige Ladungen stossen sich ab, ungleichartige ziehen sich an.
\begin{center}
    \begin{tikzpicture}
        \fill[ball color=red!60] (-2,0) circle (5mm);
        \fill[ball color=red!60] (2,0) circle (5mm);
        \draw[Latex-,very thick] (-3.5,0) -- (-2.5,0);
        \node at (-2,0) {\Huge{$+$}};
        \node at (2,0) {\Huge{$+$}};
        \draw[-Latex,very thick] (2.5,0) -- (3.5,0);


        \draw[dashed] (-2,-0.5) -- (-2,-1.5);
        \draw[dashed] (2,-0.5) -- (2,-1.5);
        \draw[Latex-Latex] (-2,-1) -- (2,-1);
        \node at (0,-0.75) {$r$};

        \node at (-2.75,0.3) {$\vec{F_1}$};
        \node at (2.75,0.3) {$\vec{F_2}$};

        \fill[ball color=red!60] (-2,-2) circle (5mm);
        \fill[ball color=blue!60] (2,-2) circle (5mm);
        \draw[-Latex,very thick] (-1.5,-2) -- (-0.5,-2);
        \node at (-2,-2) {\Huge{$+$}};
        \node at (2,-2) {\Huge{$-$}};
        \draw[Latex-,very thick] (0.5,-2) -- (1.5,-2);

        \node at (-1.25,-1.7) {$\vec{F_1}$};
        \node at (1.25,-1.7) {$\vec{F_2}$};
    \end{tikzpicture}
\end{center}

Für obige Grafik mit Ladungen $+q_1, +q_2$ resp. $+q_1, -q_2$ gilt folgender Zusammenhang:

\begin{equation*}
    |\vec{F_1}| = |\vec{F_2}| = \frac{1}{4\pi\varepsilon}\frac{|q_1q_2|}{r^2}
\end{equation*}

Die Beträge der entgegengesetzten Kräfte $\vec{F_1}$ und $\vec{F_2}$ sind jeweils gleich und proportional zum Quadrat der Distanz $r$.\\

Allgemeiner lautet das \textbf{Coulomb-Gesetz:}
\begin{equation}\label{eqn:coulomb}
    \vec{F_C} = \frac{1}{4\pi\varepsilon}\frac{q_1q_2}{r^2}
\end{equation}

$q_1$ und $q_2$ sind die Ladungen in Coulomb, $r$ der Abstand der beiden Punktladungen in Meter.
Der vorangehende Bruch, manchmal auch vereinfachend durch eine zusätzliche Konstante $k_e$ dargestellt,
beinhaltet die Konstanten $\varepsilon=\varepsilon_0\varepsilon_r$. Hierbei ist $\varepsilon_0$ die \textbf{elektrische Feldkonstante}, 
$\varepsilon_r$ die vom Medium abhängige \textbf{relative Permittivität}. Betrachtet man Ladungen im Vakuum, so gilt: $\varepsilon_r=1$ und 
folglich $\varepsilon=\varepsilon_0$.

\paragraph{Beispiel.} Zwei kleine Kugeln mit je 1 kg Masse haben genau 1 m Abstand. Wie stark müssen sie geladen sein, 
damit die Coulombkraft gleich $\vec{F}_g$ einer Kugel ist?\\
\begin{align*}
    \vec{F}_g &= \vec{F_C}\\
    m \cdot g &= k_e\frac{q_1q_2}{r^2}
\end{align*}
Die Gewichtskraft einer Kugel wird gleichgesetzt mit der Coulombkraft. Die Kugeln sind gleich stark geladen, es gilt also $q_1=q_2$.
Jetzt kann nach den Ladungen $q$ aufgelöst werden.
\begin{align*}
    q^2 &= \frac{m\cdot g \cdot r^2}{k_e}\\
    q &= \sqrt{\frac{m\cdot g \cdot r^2}{k_e}}\\
    q &= \sqrt{\frac{1 \textrm{kg}\cdot 9.81 \textrm{m/s}^2 \cdot (1 \textrm{m})^2}{8.988 \cdot 10^9 \frac{\textrm{Nm}}{\textrm{C}^2}}}\\
    \underline{\underline{q}} &= \underline{\underline{3.3 \cdot 10^{-5} \textrm{C}}}
\end{align*}
