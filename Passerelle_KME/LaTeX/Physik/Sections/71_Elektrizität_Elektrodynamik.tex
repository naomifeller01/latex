\clearpage % flushes out all floats
\subsection{Elektrodynamik}

Die Elektrodynamik befasst sich mit den Vorgängen \textit{bewegter Ladungen}. Normalerweise handelt es sich bei diesen bewegten Ladungen um Elektronen in einem Leiter. Jedoch können auch Ionen in einer Flüssigkeit 
    oder geladene Teilchen im Flug einen Strom darstellen. In der Elektrodynamik werden wird auf die Stromstärke \textit{I}, den Widerstand \textit{R} und nochmals auf die Spannung \textit{U} als Grundelemente treffen. Beginnen werden wir mit der Stromstärke \textit{I}.

\subsubsection{Die Stromstärke \textit{I}}

Bewegt sich eine elektrische Ladung in einem Leiter, entsteht elektrischer Strom \textit{I}. Der elektrische Strom \textit{I} ist ein Mass für die transportierte Ladung $\Delta Q$ pro Zeit $\Delta t$. Dies führt zur 
    folgenden Formel für die Stromstärke \textit{I}:
    \begin{equation}
        I = \frac{\Delta Q}{\Delta t} \ \left[\frac{\si{\coulomb}}{\si{\second}}\right] = [\si{\ampere}] \label{eq:Stromstärke I}
    \end{equation}

Da nun die Stromstärke bekannt ist, muss man noch wissen, in welche Richtung der Strom fliesst. Als die grundlegenden Entdeckungen in der Elektrizität gemacht wurden, war die Existenz von Atomen und Elektronen noch 
    unbekannt. Die Stromrichtung wurde zu dieser Zeit willkürlich festgelegt. Heute unterscheidet man die \textit{physikalische} und die \textit{konventionelle} Stromrichtung: Bei der physikalischen Stromrichtung bewegen sich die Elektronen vom Minus- zum Pluspol, bei der konventionellen Stromrichtung gehen die Elektronen vom Plus- zum Minuspol. \textbf{Wir verwenden die konventionelle Stromrichtung}.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Wir untersuchen einen ein Meter langen Kupferdraht für Hausinstallationen (Querschnitt $1.5\si{\mm}^2$). Kupfer hat ein Leitungselektron pro Atom, seine Dichte beträgt 
    $\rho = 8'960\frac{\si{\kg}}{\si{\metre}^3}$, die molare Masse $M = 0.063546\frac{\si{\kg}}{\si{\mole}}$. \\\hspace*{\fill}

\textit{Gesucht:}
\begin{enumerate}[label=\alph*)]
    \item Wie viele Leitungselektronen enthält der Draht?
    \item Wie gross ist die Gesamtladung \textit{Q} der Leitungselektronen?
    \item Wie lange dauert es, bis sich ein Elektron durch den Draht bewegt hat, wenn die Stromstärke den maximal zulässigen Wert von 13 Ampère aufweist`?
    \item Wie gross ist die Geschwindigkeit (Driftgeschwindigkeit) der Leitungselektronen?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}[label=\alph*)]
    \item \begin{align*}
              N = N_A \cdot \frac{m}{M} = N_A \cdot \frac{\rho \cdot V}{M} &= N_A \cdot \frac{\rho \cdot A \cdot l}{M} \\
                                                                           &= \frac{6.02 \cdot 10^{23} \cdot 8'960\frac{\si{\kg}}{\si{\metre}^3} \cdot 1.5 \cdot 10^{-6}\si{\metre}^2 \cdot 1\si{\metre}}{0.063546\frac{\si{\kg}}{\si{\mole}}} \\
                                                                           &= \underline{\underline{1.27 \cdot 10^{23} \ \textnormal{Elektronen}}}
          \end{align*}
    \item $Q = N \cdot Q_{1 \, Elektron} = 1.27 \cdot 10^{23} \cdot 1.602 \cdot 10^{-19}\si{\coulomb} = \underline{\underline{2.04 \cdot 10^4\si{\coulomb}}}$
    \item $I = \frac{\Delta Q}{\Delta t} \ \Rightarrow \ t = \frac{Q}{I} = \frac{2.04 \cdot 10^4\si{\coulomb}}{13\si{\ampere}} = \underline{\underline{1'569\si{\second}}}$
    \item $v = \frac{s}{t} = \frac{1\si{\metre}}{1'569\si{\second}} = \underline{\underline{6.37 \cdot 10^{-4}\frac{\si{\metre}}{\si{\second}}}}$
\end{enumerate}

\subsubsection{Gleichstrom und Wechselstrom}

Ist die elektrische Stromstärke zeitlich konstant, so spricht man von einem Gleichstrom (direct current, DC). In der Praxis haben wir es häufig mit einem zeitlich veränderlichen Strom zu tun: In unserem Stromnetz 
    haben wir den sinusförmigen Wechselstrom (alternating current, AC). Das heisst, dass sich die elektrische Stromstärke in Form einer Sinusfunktion ändert (Abbildung \ref{fig:Diagramm einer Wechselspannung}).

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Axis
            \draw[->,thick,MyBlack] (2,0.5) -- (8,0.5) node[anchor=north, shift={(0mm,-1mm)}, text=MyBlack] {$t \, [\si{\ms}]$}; % x-axis
            \draw[thick,MyBlack] (4,0.6) -- (4,0.4) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$10$}; % 10
            \draw[thick,MyBlack] (6,0.6) -- (6,0.4) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$20$}; % 20
            \draw[->,thick,MyBlack] (2,-1.5) -- (2,2.5) node[anchor=east, shift={(-1mm,0mm)}, text=MyBlack] {$I$}; % y-axis

            % Sin & Cos wave
            \draw[very thick,MyRed] (2,0.5) sin (3,2) cos (4,0.5) sin (5,-1) cos (6,0.5) sin (7,2);

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Diagramm einer Wechselspannung.}
    \label{fig:Diagramm einer Wechselspannung}
\end{figure}

Das Symbol für den Gleichstrom ist \begin{tikzpicture}[scale = 0.05]
                                       \draw (0,0) sin (1,1) cos (2,0) sin (3,-1) cos (4,0);
                                       \draw (2,0) circle (2.5);
                                   \end{tikzpicture}, für den Wechselstrom \begin{tikzpicture}[scale = 0.05]
                                                                               \draw (0.5,1) -- (3.5,1);
                                                                               \draw (0.5,-1) -- (3.5,-1);
                                                                               \draw (2,0) circle (2.5);
                                                                           \end{tikzpicture}.

\subsubsection{Die Wirkung des elektrischen Stroms}

Elektrischer Strom kann für verschiedene Zwecke gebraucht werden. Vom Elektromobil bis zur Stereoanlage vermag die Elektrizität verschiedene Aufgaben erfüllen. Schaut man jedoch genauer hin, so sieht man, dass drei 
    Grundwirkungen des elektrischen Stroms unterschieden werden können.

\subsubsubsection{Die Wärmewirkung des Stroms}

Der elektrische Strom kann einen Leiter erwärmen. Die Temperatur kann so weit steigen, dass das Objekt zu glühen beginnt, so z.B. die Glühbirne. Hier erwärmt der Strom den dünnen Draht im Innern auf über 
    $2'500\si{\degreeCelsius}$, was ihn zum Glühen bringt. Damit der Draht nicht schmilzt, werden Metalle mit einem sehr hohen Schmelzpunkt verwendet, wie z.B. Osmium oder Wolfram.

\subsubsubsection{Die magnetische Wirkung des Stroms}

Über die Gesetze des Magnetismus und besonders über den Zusammenhang zwischen dem elektrischen Feld und Magnetfeld wusste man zu Beginn des 19. Jahrhunderts nichts. Im Jahre 1820 machte der dänische Physiker Hans 
    Christian Oersted  eine entscheidende Entdeckung: Er beobachtete, dass sich Magnetnadeln drehen, wenn in ihrer Nähe ein elektrischer Strom eingeschaltet wird. Das heisst, stellt man in der Umgebung eines geraden, stromdurchflossenen Leiters mehrere Kompassnadeln auf, so werden sie tangential zu Kreisen ausgerichtet, die konzentrisch um den Leiter verlaufen. Ändert man die Richtung des Stromflusses, so kehren auch die Nadeln ihre Richtung um. Man kann also sagen, dass elektrische Ströme eine Kompassnadel ablenken, sie also \textit{Magnetfelder erzeugen}.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            \newcommand\MagneticFieldLine[2]{
                \draw[
                    very thick,
                    MyYellow!60!MyBrown,
                    decoration={
                        markings,
                        mark=at position 0.1 with {\arrow{>}},
                        mark=at position 0.6 with {\arrow{>}}
                        },
                    postaction={decorate}
                    ] (#1,#2) ellipse (1.25cm and 0.35cm);
            }
    
            \newcommand\VerticalWire[3]{
                % #1 = x start (bottom)
                % #2 = y start (bottom)
                % #3 = y stop (top)
                \draw[draw=none,fill=MyOrange!70!MyBrown] (#1-0.05,#2) rectangle (#1+0.05,#3);
                \node[draw=none,fill=none,text=MyRed] at (#1,#2-0.35) {\huge $+$};
                \node[draw=none,fill=none,text=MyDarkBlue] at (#1,#3+0.15) {\huge $-$};
            }
    
            % Left side
            \VerticalWire{1.5}{-2.5}{2.5}
            \foreach \y in {-1.5,0,1.5}{
                \MagneticFieldLine{1.5}{\y}
            }
            %% make wire go over back part of field line
            \draw[draw=none,fill=MyOrange!70!MyBrown] (1.45,1.75) rectangle (1.55,1.95); % top
            \draw[draw=none,fill=MyOrange!70!MyBrown] (1.45,0.25) rectangle (1.55,0.45); % middle
            \draw[draw=none,fill=MyOrange!70!MyBrown] (1.45,-1.25) rectangle (1.55,-1.05); % bottom
    
            % Dividing line
            \draw[very thick,draw=MyGrey,fill=none] (5,2.5) -- (5,-2.5);
    
            % Right side
            \node[inner sep=0pt] at (8.4,0) {\includegraphics[width=3.5cm]{rhr.pdf}};
            \VerticalWire{8.5}{-2.5}{2.5}
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Magnetfeldlinien und Rechte-Hand-Regel bei einem geraden, stromdurchflossenen Leiter.}
\end{figure}

Die Ablenkung der Nadeln erfolgt nach der sogenannten \textit{Recht-Hand-Regel}: Umfasst man den Leiter mit der rechten Hand, sodass der Daumen in die Flussrichtung des Stromes zeigt, so zeigen die anderen Finder 
    die Richtung des Magnetfeldes an.

\subsubsubsection{Die chemische Wirkung des Stroms}

Nicht nur Metalle, auch Säuren, Laugen und Lösungen von Salzen in Wasser leiten den elektrischen Strom. Weit bekannt ist die Elektrolyse, welche Wasser in Sauerstoff und Wasserstoff aufspaltet. \\\hspace*{\fill}

Als weiterer Begriff wird jetzt der \textit{elektrische Widerstand R} eingeführt. Verschiedene Geräte leiten den Strom unterschiedlich gut. Ein grosser Strom bedeutet eine hohe Leitfähigkeit, was gleichgesetzt mit 
    dem Ausdruck \glqq das Gerät hat einen \textit{kleinen Widerstand}\grqq{} werden kann. Oft wird mit dem Wort \textit{Widerstand} auch der Leiter (das Gerät) selbst bezeichnet. \\\hspace*{\fill}

Das führt uns nun zum Widerstand eines elektrischen Leiters. Der \textit{elektrische Widerstand} eines Leiters hängt von den geometrischen Abmessungen und vom Material ab. Verdoppelt man z.B. die Länge \textit{l} 
    eines Leiters (z.B. eines Drahtes), so verdoppelt sich der elektrische Widerstand \textit{R}, weil der elektrische Strom bei konstant gehaltener Spannung halbiert wird. Verdoppelt man hingegen die Querschnittsfläche \textit{A}, so wird der Strom bei konstanter Spannung verdoppelt und der Widerstand halbiert. Das kann man auf einen Satz bringen: Der Widerstand \textit{R} eines Leiters ist proportional zu seiner Länge \textit{l} und umgekehrt proportional zu seinem Querschnitt. Dies können wir in eine Formel bringen:
    \begin{equation}
        R = \rho \cdot \frac{l}{A} \ [\si{\ohm}] \label{eq:Widerstand R}
    \end{equation}

Der spezifische Widerstand $\rho$ ist vom Material und von der Temperatur abhängig. \textbf{Achtung}: Der spezifische Widerstand $\rho$ ist nicht mit der Dichte $\rho$ (gleiches Symbol) zu verwechseln.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Kupferdraht sei 87 Meter lang. \\\hspace*{\fill}

\textit{Gesucht:} Welche Fläche darf er haben, wenn der Widerstand nicht unter $100\si{\ohm}$ liegen soll? \\\hspace*{\fill}

\textit{Lösung:}
\[
    R = \rho \cdot \frac{l}{A} \ \Rightarrow \ A = \frac{\rho \cdot l}{R} = \frac{1.754 \cdot 10^{-8}\si{\ohm} \cdot \si{\metre} \cdot 87\si{\metre}}{100\si{\ohm}} = \underline{\underline{1.53 \cdot 10^{-8}\si{\metre}^2}}
\]

\subparagraph{Beispiel 2}

\textit{Gegeben:} An einer unterirdisch verlegten, zweiadrigen Telefonleitung aus Kupfer ist ein Defekt aufgetreten (siehe Abbildung unten). Da man für die Reparatur das Kabel nicht auf der ganzen Länge ausgraben 
    will, wird der Widerstand zwischen A und B gemessen und $R = 17.1\si{\ohm}$ gefunden.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}

            % Ground
            \draw[thick,MyBlack] (-1,0) -- (11,0);
            \foreach \x in {-1,-0.75,...,0.25,1.5,1.75,...,8.75,10,10.25,...,11}{
                \draw[thick,MyBlack] (\x,0) -- (\x-0.25,-0.25);
            }

            % Telephone lines
            \draw[very thick,MyGrey] (1,0) -- (1,-1) -- (9,-1) -- (9,0); % inner
            \node[draw=none,fill=none,text=MyBlack] at (1,0.25) {B};
            \draw[very thick,MyGrey] (0.5,0) -- (0.5,-1.5) -- (9.5,-1.5) -- (9.5,0); % outer
            \node[draw=none,fill=none,text=MyBlack] at (0.5,0.25) {A};

            % Red cross
            \draw[very thick,MyRed] (7,-0.5) -- (6.5,-2);
            \draw[very thick,MyRed] (6.5,-0.5) -- (7,-2);
            \node[draw=none,fill=none,text=MyBlack] at (6.75,-2.25) {Kurzschluss};

        \end{tikzpicture}
    \end{center}

\end{figure}

\textit{Gesucht:} Wo ist der Kurzschluss, wenn die Drähte einen Querschnitt von je $0.5\si{\mm}^2$ haben? \\\hspace*{\fill}

\textit{Lösung:}
\[
    R = \rho \cdot \frac{l}{A} \ \Rightarrow \ l = \frac{R \cdot A}{\rho} = \frac{17.1\si{\ohm} \cdot 0.5 \cdot 10^{-6}\si{\metre}^2}{1.754 \cdot 10^{-8}\si{\ohm} \cdot \si{\metre}} = 487.46\si{\metre}
\]
Da zwei Kabel vorhanden sind, ist der Defekt $\frac{487.46\si{\metre}}{2} = \underline{\underline{243.73\si{\metre}}}$ von A und B entfernt.

\subsubsection{Das Ohm'sche Gesetz}

In einem metallischen Leiter, bei dem Form und Grösse konstant sind, sind Stromstärke \textit{I} und Spannung \textit{U} proportional zueinander, solange die Temperatur konstant gehalten wird. Diese Proportionalität 
    zwischen der Stromstärke \textit{I} und Spannung \textit{U} nennt man das Ohm'sche Gesetz. Dieses Gesetz wurde 1826 von Georg Simon Ohm entdeckt. Das Ohm'sche Gesetz kann mit der folgenden Formel beschrieben werden:
    \begin{equation}
        U = R \cdot I \ [\si{\volt}] \label{eq:Ohmsche Gesetz}
    \end{equation}

Da jedoch der Widerstand von der Temperatur abhängt, kann das Ohm'sche Gesetz nicht als universelles Gesetz angeschaut werden; es beschreibt lediglich eine bestimmte Klasse von Materialien. Das Material \glqq Konstantan\grqq{} ändert seinen Widerstand nur sehr wenig mit der Temperatur, wohingegen bei einem Wolframdraht in einer Halogenlampe sich nach dem Einschalten der Lampe die Charakteristiken sehr stark ändern.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Kupferkabel für das $110\si{\kilo\volt}$-Netz hat einen Durchmesser von $630\si{\mm}^2$ und sei $5.7\si{\km}$ lang. Es wird von $652\si{\ampere}$ durchflossen. \\\hspace*{\fill}

\textit{Gesucht:}
\begin{enumerate}[label=\alph*)]
    \item Wie gross ist der Widerstand des Kabels?
    \item Welche Spannung liegt im Kabel vor?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}[label=\alph*)]
    \item $R = \rho \cdot \frac{l}{A} = 1.754 \cdot 10^{-8}\si{\ohm} \cdot \si{\metre} \cdot \frac{5'700\si{\metre}}{630 \cdot 10^{-6}\si{\metre}} = \underline{\underline{0.159\si{\ohm}}}$
    \item $U = R \cdot I = 0.159\si{\ohm} \cdot 652\si{\ampere} = \underline{\underline{103.47\si{\volt}}}$
\end{enumerate}