\clearpage % flushes out all floats
\section{Thermodynamik}

Die Wärmelehre oder Thermodynamik ist ein Teilgebiet der Physik, welches sich mit der Temperatur von Körpern, der Zufuhr und Abgabe von Wärme und den damit verbundenen Temperatur- und Volumenänderungen, den 
    Aggregatzuständen und ihren Änderungen, der Wärmeübertragung und den Wärmekraftmaschinen (Verbrennungsmotoren, Turbinen, Dampfmaschinen) beschäftigt.

\subsection{Temperatur und Wärme}

Die \textit{Temperatur} ist eine \textbf{Zustandsgrösse}, die den thermodynamischen Zustand -- das heisst den Wärmezustand -- eines Körpers beschreibt. Demgegenüber ist die \textit{Wärme} ist eine 
    \textbf{Prozessgrösse}, die den Vorgang der Erwärmung oder Abkühlung eines Körpers beschreibt. Wärme muss einem Körper von aussen zugeführt werden, um ihn zu erwärmen, also um seine Temperatur bzw. seine sogenannte \textbf{innere Energie} zu erhöhen. Wärme wird dem Körper bei einer Abkühlung entzogen, er wird kälter, seine Temperatur nimmt ab und damit auch seine innere Energie. \\\hspace*{\fill}

Die innere Energie ist die gesamte Bewegungs- und Bindungsenergie aller in einem physikalischen Körper enthaltenen Atome oder Moleküle. Durch z.B. mechanische Reibungsarbeit an einem Gegenstand nimmt seine innere 
    Energie zu, was die Temperatur erhöht. \\\hspace*{\fill}

Mit dem absoluten Nullpunkt wird eine neue, absolute Temperaturskala, die Kelvin-Skala, eingeführt. Für Kelvin-Temperaturen wird das Symbol \textit{T} verwendet. Es gilt:
    \begin{align}
        0\si{\kelvin} &= -273.15\si{\degreeCelsius} \label{eq:Absoluter Nullpunkt} \\
        0\si{\degreeCelsius} &= 273.15\si{\kelvin}
    \end{align}

\subsubsection{Wärmeausdehnung von Festkörpern und Flüssigkeiten}

Bei der Erwärmung dehnen sich Flüssigkeiten aus, so wie man das vom Quecksilberthermometer kennt. Dasselbe gilt für Gase und Festkörper. So haben z.B. Brücken oder Eisenbahnschienen je nach Temperatur eine leicht 
    unterschiedliche Länge. \\\hspace*{\fill}

Erwärmt man zum Beispiel einen Stab der Länge $l_0$ von einer Anfangstemperatur $\vartheta_0$ auf eine Endtemperatur von $\vartheta$, so zeigt sich bei der Beobachtung eine Verlängerung des Stabes um $\Delta l$. \hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Top Cylinder
            \draw[draw=none,fill=MyLightGrey] (1.5,2) -- (7.5,2) -- (7.5,1) -- (1.5,1) -- cycle;
            %% Ellipses
            \draw[color=MyGrey,very thick,fill=MyLightGrey] (1.5,1.5) ellipse (0.125 and 0.5); % Left
            \draw [color=MyGrey,very thick,fill=MyLightGrey] plot [smooth, tension=1.75] coordinates {(7.5,2) (7.625,1.5) (7.5,1)}; % Left (full line)
            \draw [color=MyGrey,very thick,dashed] plot [smooth, tension=1.75] coordinates {(7.5,2) (7.375,1.5) (7.5,1)}; % Left (dotted line)
            %% Lines
            \draw[color=MyGrey,very thick] (1.5,2) -- (7.5,2); % Top
            \draw[color=MyGrey,very thick] (1.5,1) -- (7.5,1); % Bottom
            %% Text
            \node[color=MyBlack] at (3.5,2.2) {$\text{Temperatur } \vartheta_0$};

            % Bottom Cylinder
            \draw[draw=none,fill=MyLightGrey!80!MyRed] (1.5,0) -- (8.5,0) -- (8.5,-1) -- (1.5,-1) -- cycle;
            %% Ellipses
            \draw[color=MyGrey,very thick,fill=MyLightGrey!80!MyRed] (1.5,-0.5) ellipse (0.125 and 0.5); % Left
            \draw [color=MyGrey,very thick,fill=MyLightGrey!80!MyRed] plot [smooth, tension=1.75] coordinates {(8.5,0) (8.625,-0.5) (8.5,-1)}; % Left (full line)
            \draw [color=MyGrey,very thick,dashed] plot [smooth, tension=1.75] coordinates {(8.5,0) (8.375,-0.5) (8.5,-1)}; % Left (dotted line)
            %% Lines
            \draw[color=MyGrey,very thick] (1.5,0) -- (8.5,0); % Top
            \draw[color=MyGrey,very thick] (1.5,-1) -- (8.5,-1); % Bottom
            %% Text
            \node[color=MyBlack] at (3.5,0.2) {$\text{Temperatur } \vartheta$};

            % Length difference
            \draw[color=MyBlack!75,thick] (1.5,3) -- (1.5,-2); % vertical line left
            \draw[color=MyBlack!75,thick] (8.5,3) -- (8.5,-2); % vertical line right
            \draw[color=MyBlack!75,<->,thick] (1.5,-1.85) -- (8.5,-1.85) node[anchor=center, shift={(-35mm,2.5mm)}, text=MyBlack] {$l$}; % l (bottom)
            \draw[color=MyBlack!75,thick] (7.5,3) -- (7.5,1); % vertical line 'middle'
            \draw[color=MyBlack!75,<->,thick] (1.5,2.85) -- (7.5,2.85) node[anchor=center, shift={(-30mm,4mm)}, text=MyBlack] {$l_0$}; % l_0 (top left)
            \draw[color=MyBlack!75,<->,thick] (7.5,2.85) -- (8.5,2.85) node[anchor=center, shift={(-5mm,4mm)}, text=MyBlack] {$\Delta l$}; % \Delta l (top right)

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Thermische Längenausdehnung eines Stabs.}
\end{figure}
\newpage

Falls $\Delta\vartheta = \vartheta - \vartheta_0$ nicht allzu gross ist, verhält sich $\Delta l$ proportional zur ursprünglichen Stablänge $l_0$ und zur Temperaturzunahme $\Delta\vartheta$:
    \begin{equation}
        \Delta l = \alpha \cdot l_0 \cdot \Delta\vartheta \label{eq:Wärmeausdehnung Festkörper Delta l}
    \end{equation}

Für die gesamte Stablänge gilt:
    \begin{equation}
        l = l_0 + \Delta l = l_0 + l_0 \cdot \alpha \cdot \Delta\vartheta = l_0 \cdot (1 + \alpha \cdot \Delta\vartheta) \label{eq:Längenausdehnung Festkörper}
    \end{equation}

Besonderes Augenmerk muss darauf gelegt werden, welches $\Delta$ verwendet wird: $\Delta\vartheta$ bezeichnet den Temperaturunterschied in $\si{\degreeCelsius}$, $\Delta T$ den Temperaturunterschied in 
    $\si{\kelvin}$. \\\hspace*{\fill}

Bei der Formel (\ref{eq:Längenausdehnung Festkörper}) bezeichnet $\alpha$ den linearen Ausdehnungskoeffizient mit der Einheit $\alpha = \left[\frac{1}{\si{\kelvin}}\right]$ oder 
    $\left[\frac{1}{\si{\degreeCelsius}}\right]$. Er gibt die relative Verlängerung bei einer Erwärmung um $1\si{\degreeCelsius}$ an. Hier ein paar Beispiele für lineare thermische Ausdehnungskoeffizienten in $\frac{1}{\si{\kelvin}}$ (bei $20\si{\degreeCelsius}$):
    \begin{center}
        \begin{tabular}{c|c|c|c} % Alignment of Columns
        Quarzglas & Zink & Eisen & Glaskeramik \\
        \hline
        $0.5 \cdot 10^{-6}$ & $26 \cdot 10^{-6}$ & $12.2 \cdot 10^{-6}$ & $<0.1 \cdot 10^{-6}$
        \end{tabular}
    \end{center}

Die meisten Stoffe dehnen sich bei höheren Temperaturen stärker aus als bei tieferen, weshalb die oben angegebene Formel (\ref{eq:Längenausdehnung Festkörper}) und die Werte der linearen Ausdehnungskoeffizienten nur 
    in einem beschränkten Temperaturintervall gelten.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine 20 Meter lange Eisenbahnschiene aus Eisen ist jahreszeitlichen Temperaturunterschieden zwischen $-15\si{\degreeCelsius}$ und $40\si{\degreeCelsius}$ unterworfen. \\\hspace*{\fill}

\textit{Gesucht:} Welche Längenänderung erfährt sie? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \Delta l = \alpha \cdot l_0 \cdot \Delta\vartheta = 12.2 \cdot 10^{-6}\si{\frac{1}{\degreeCelsius}} \cdot 20\si{\metre} \cdot 55\si{\degreeCelsius} = \underline{\underline{0.013\si{\metre}}}
\]

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Glasstab hat bei der Temperatur von schmelzendem Eis eine Länge von $412.5\si{\mm}$. Er verlängert sich um $0.329\si{\mm}$, wenn er in Wasserdampf von $98.5\si{\degreeCelsius}$ gebracht wird. \\\hspace*{\fill}

\textit{Gesucht:} Wie gross ist der lineare Ausdehnungskoeffizient? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \Delta l = \alpha \cdot l_0 \cdot \Delta\vartheta \ \Rightarrow \ \alpha = \frac{\Delta l}{l_0 \cdot \Delta\vartheta} = \frac{3.29 \cdot 10^{-4}\si{\metre}}{0.4125\si{\metre} \cdot 98.5\si{\degreeCelsius}} = \underline{\underline{8.097 \cdot 10^{-6}\si{\frac{1}{\degreeCelsius}}}}
\]

\subparagraph{Beispiel 3}

\textit{Gegeben:} Ein Schmied will einen stählernen Reifen auf ein Rad aufziehen. Der Durchmesser des Rades beträgt $0.74\si{\metre}$, der innere Durchmesser des Reifens aber nur $0.735\si{\metre}$. Die Umgebungstemperatur beträgt $15\si{\degreeCelsius}$, der lineare Ausdehnungskoeffizient von Stahl beträgt 8.097 $16 \cdot 10^{-6}\si{\frac{1}{\degreeCelsius}}$. \\\hspace*{\fill}

\textit{Gesucht:} Auf welche Temperatur muss der Schmied den Reifen erwärmen, damit er ihn auf das Rad aufziehen kann? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \Delta l = \alpha \cdot l_0 \cdot \Delta\vartheta - 15\si{\degreeCelsius} \ \Rightarrow \ \Delta\vartheta = \frac{\Delta l}{l_0 \cdot \alpha} + 15\si{\degreeCelsius} = \frac{0.005\si{\metre}}{0.735\si{\metre} \cdot 16 \cdot 10^{-6}\si{\frac{1}{\degreeCelsius}}} + 15\si{\degreeCelsius} = \underline{\underline{440.17\si{\degreeCelsius}}}
\]
\newpage

\subsubsubsection{Volumenausdehnung eines Festkörpers}

Die Ausdehnung eines festen Körpers erfolgt allseitig. Isotrope Körper wie etwa Fensterglas dehnen sich dabei in allen Richtungen im gleichen Verhältnis aus, während bei anisotropen Körpern 
    -- z.B. der Bergkristall -- die Ausdehnung in verschiedenen Richtungen unterschiedlich ist. \\\hspace*{\fill}

Im Folgenden wird ein homogener, isotroper, quaderförmiger Festkörper, der bei einer Temperatur $\vartheta_0$ die Kantenlängen $a_0$, $b_0$ und $c_0$ aufweist 
    (Abbildung \ref{fig:Volumenausdehnung eines isotropen Festkörpers}), untersucht. Wird dieser Körper auf eine Temperatur $\vartheta$ erwärmt, so verlängert sich jede dieser Kantenlängen gemäss der Formel für die lineare Wärmeausdehnung, und es gilt:
    \begin{align*}
        V &= a \cdot b \cdot c = (a_0 + \Delta a) \cdot (b_0 + \Delta b) \cdot (c_0 + \Delta c) \\
          &= a_0 \cdot (1 + \alpha \cdot \Delta\vartheta) \cdot b_0 \cdot (1 + \alpha \cdot \Delta\vartheta) \cdot c_0 \cdot (1 + \alpha \cdot \Delta\vartheta) \\
          &= \underbrace{a_0 \cdot b_0 \cdot c_0}_{V_0} \cdot (1 + \alpha \cdot \Delta\vartheta)^3 \\
          &= V_0 \cdot (1 + 3 \cdot \alpha \cdot \Delta\vartheta + \underbrace{3 \cdot a^2 \cdot \Delta\vartheta^2}_{\rightarrow 0} + \underbrace{\alpha^3 \cdot \Delta\vartheta^3}_{\rightarrow 0}) \\
          &= V_0 \cdot (1 + \underbrace{3 \cdot \alpha}_{\gamma} \cdot \Delta\vartheta)
    \end{align*}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Inner smaller block
            \draw[color=MyBlack,very thick] (4,1.5) -- (5,1.5) -- (5,0.5) -- (4,0.5) -- cycle node[anchor=center, shift={(5mm,-12mm)}, text=MyBlack] {$a_0$} node[anchor=center, shift={(-2mm,-3.5mm)}, text=MyBlack] {$b_0$}; % front face
            \draw[color=MyBlack,very thick] (4.5,2) -- (5.5,2) -- (5.5,1); % back face (full line)
            \draw[color=MyBlack,very thick,dashed] (4.5,2) -- (4.5,1) -- (5.5,1); % back face (dashed line)
            \draw[color=MyBlack,very thick,dashed] (4,0.5) -- (4.5,1); % diagonal left bottom (dashed line)
            \draw[color=MyBlack,very thick] (4,1.5) -- (4.5,2); % diagonal left top (full line)
            \draw[color=MyBlack,very thick] (5,1.5) -- (5.5,2); % diagonal right top (full line)
            \draw[color=MyBlack,very thick] (5,0.5) -- (5.5,1) node[anchor=center, shift={(0mm,-3mm)}, text=MyBlack] {$c_0$}; % diagonal right bottom (full line)

            % Outer bigger block
            \draw[color=MyOrange,very thick] (3,1.5) -- (5,1.5) -- (5,-0.5) -- (3,-0.5) -- cycle node[anchor=center, shift={(10mm,-22mm)}, text=MyBlack] {$a$} node[anchor=center, shift={(-2mm,-10mm)}, text=MyBlack] {$b$}; % front face
            \draw[color=MyOrange,very thick] (4.5,3) -- (6.5,3) -- (6.5,1); % back face (full line)
            \draw[color=MyOrange,very thick,dashed] (4.5,3) -- (4.5,1) -- (6.5,1); % back face (dashed line)
            \draw[color=MyOrange,very thick,dashed] (3,-0.5) -- (4.5,1); % diagonal left bottom (dashed line)
            \draw[color=MyOrange,very thick] (3,1.5) -- (4.5,3); % diagonal left top (full line)
            \draw[color=MyOrange,very thick] (5,1.5) -- (6.5,3); % diagonal right top (full line)
            \draw[color=MyOrange,very thick] (5,-0.5) -- (6.5,1) node[anchor=center, shift={(-5mm,-8mm)}, text=MyBlack] {$c$}; % diagonal right bottom (full line)

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Volumenausdehnung eines isotropen Festkörpers.}
    \label{fig:Volumenausdehnung eines isotropen Festkörpers}
\end{figure}

Die Glieder $3 \cdot \alpha^2 \cdot \Delta\vartheta^2$ und $\alpha^3 \cdot \Delta\vartheta^3$ werden in der Rechnung vernachlässigt, weil sie viel kleiner als $3 \cdot \alpha \cdot \Delta\vartheta$ sind. Die Formel 
    zur Berechnung der Volumenausdehnung von Festkörpern lautet demnach:
    \begin{equation}
        \Delta V = V_0 \cdot 3\alpha \cdot \Delta\vartheta = V_0 \cdot \gamma \cdot \Delta\vartheta \label{eq:Volumenausdehnung Festkörper}
    \end{equation}

Die Einheit von $\gamma$ (Gamma) ist $\frac{1}{\si{\kelvin}}$ oder $K^{-1}$.
\newpage

\subsubsubsection{Volumenausdehnung von Flüssigkeiten}

Die Formel \ref{eq:Volumenausdehnung Festkörper} gilt nicht nur für feste Körper, sondern auch für Flüssigkeiten, dessen Volumen mit steigender Temperatur zunimmt. Im Wesentlichen ist die temperaturbedingte 
    Volumenzunahme von Flüssigkeiten darauf zurückzuführen, dass die Geschwindigkeit der Atome beziehungsweise Moleküle in der Flüssigkeit zunimmt. Die Teilchen beanspruchen so mehr Raum innerhalb der Flüssigkeit, das Volumen nimmt zu. \\\hspace*{\fill}

Etwas ausführlicher notiert lautet die Formel zur Berechnung der Volumenausdehnung von Flüssigkeiten folgendermassen:
    \begin{equation}
        V = V_0 + \Delta V = V_0 + V_0 \cdot 3\alpha \cdot \Delta\vartheta = V_0 \cdot (1 + \overbrace{3\alpha}^{\gamma} \cdot \Delta\vartheta) \label{eq:Volumenausdehnung Flüssigkeiten}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Quecksilberthermometer enthält bei $0\si{\degreeCelsius}$ eines Quecksilbermenge von $0.4\si{\metre}^3$. Der Volumenausdehnungskoeffizient ist $\gamma_{Hg} = 2 \cdot 10^{-4} C^{-1}$. \\\hspace*{\fill}

\textit{Gesucht:} Welchen Durchmesser muss die Kapillare haben, damit bei $\Delta\vartheta = 1\si{\degreeCelsius}$ ein Ansteigen der Quecksilbersäule von $1\si{\mm}$ entspricht? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \Delta V &= V_0 \cdot 3\alpha \cdot \Delta\vartheta = 0.4 \cdot 10^{-6}\si{\metre}^3 \cdot 2 \cdot 10^{-4} C^{-1} \cdot 1\si{\degreeCelsius} = 8 \cdot 10^{-11}\si{\metre}^3 \ \text{(Anmerkung: $3\alpha = \gamma$)} \\
    V_{Zylinder} &= A \cdot h = r^2 \cdot \pi \cdot h = \left(\frac{d}{2}\right)^2 \cdot \pi \cdot h \\
    \Rightarrow \ d &= \sqrt{\frac{V_{Zylinder}}{\pi \cdot h}} \cdot 2 = \sqrt{\frac{8 \cdot 10^{-11}\si{\metre}^3}{\pi \cdot 0.001\si{\metre}}} = \underline{\underline{3.19 \cdot 10^{-4}\si{\metre}}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Öltank aus Stahlblech mit $V_0 = 5'000\si{\litre}$ wird bei $20\si{\degreeCelsius}$ vollständig mit Heizöl gefüllt. Der Volumenausdehnungskoeffizient von Heizöl ist $\gamma_{\textnormal{\textit{Heizöl}}} = 8.9 \cdot 10^{-4} C^{-1}$, der lineare Ausdehnungskoeffizient von Stahl $\alpha_{Stahl} = 13 \cdot 10^{-6} C^{-1}$. \\\hspace*{\fill} % \textnormal is needed for proper display of umlauts

\textit{Gesucht:} Was passiert, wenn die Temperatur auf $35\si{\degreeCelsius}$ steigt? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \Delta V_{Tank} &= V_0 \cdot 3\alpha \cdot \Delta\vartheta = 5\si{\metre}^3 \cdot 3 \cdot 13 \cdot 10^{-6} C^{-1} \cdot 15\si{\degreeCelsius} = 0.002925\si{\metre}^3 \\
    \Delta V_{\textnormal{\textit{Heizöl}}} &= V_0 \cdot 3\alpha \cdot \Delta\vartheta = 5\si{\metre}^3 \cdot 8.9 \cdot 10^{-4} C^{-1} \cdot 15\si{\degreeCelsius} = 0.06675\si{\metre}^3 \\
    \Rightarrow \ \Delta_{\textnormal{\textit{Öl,Tank}}} &= \Delta V_{\textnormal{\textit{Heizöl}}} - \Delta V_{Tank} = 0.06675\si{\metre}^3 - 0.002925\si{\metre}^3 = \underline{\underline{0.064\si{\metre}^3 (= 63.83\si{\litre}) \ \text{treten aus.}}} % \textnormal is needed for proper display of umlauts
\end{align*}
\newpage

\subsubsubsection{Veränderung der Dichte eines Körpers}

Mit dem Volumen \textit{V} ändert sich auch die Dichte $\rho$ eines Körpers, egal ob er fest oder flüssig ist. Wird dieser um $\Delta\vartheta$ erwärmt, so nimmt sein Volumen von $V_0$ auf \textit{V} zu und seine 
    Dichte von $\rho_0 = \frac{m}{V_0}$ auf $\rho = \frac{m}{V}$ ab:
    \begin{equation}
        \rho = \frac{m}{V} = \frac{m}{V_0 + \Delta V} = \frac{m}{V_0} \cdot \frac{1}{1 + \gamma \cdot \Delta\vartheta} = \rho_0 \cdot \frac{1}{1 + \gamma \cdot \Delta\vartheta} \label{eq:Veränderung Dichte Körper}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Bei $18\si{\degreeCelsius}$ beträgt die Dichte von Messing $8.1\si{\gram/\cm}^3$. Messing hat einen linearen Ausdehnungskoeffizienten von $\alpha_{Messing} = 5.7 \cdot 10^{-5} C^{-1}$. \\\hspace*{\fill}

\textit{Gesucht:} Welche Dichte besitzt Messing bei $-35\si{\degreeCelsius}$? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \rho = \rho_0 \cdot \frac{1}{1 + \gamma \cdot \Delta\vartheta} = 8.1 \cdot 10^3\si{\kg/\metre}^3 \cdot \frac{1}{1 + 3 \cdot 5.7 \cdot 10^{-5} C^{-1} \cdot (-53\si{\degreeCelsius})} = \underline{\underline{8'174.08\si{\kg/\metre}^3}}
\]
    
\subparagraph{Beispiel 2}

\textit{Gegeben:} Heizöl hat bei $20\si{\degreeCelsius}$ eine Dichte von $840\si{\kg/\metre}^3$ und einen Volumenausdehnungskoeffizienten $\gamma$ von $8.9 \cdot 10^{-4} C^{-1}$. \\\hspace*{\fill}

\textit{Gesucht:} Wieviel Heizöl (in Kilogramm) erhält man, wenn man $10^4$ Liter einmal bei $0\si{\degreeCelsius}$ und einmal bei $35\si{\degreeCelsius}$ kauft? \\\hspace*{\fill}

\textit{Lösung:} \\
\underline{Bei $0\si{\degreeCelsius}$:}
\begin{align*}
    \rho = \rho_0 \cdot \frac{1}{1 + \gamma \cdot \Delta\vartheta} \cdot 10^4\si{\litre} = 840\si{\kg/\metre}^3 \cdot \frac{1}{1 + 8.9 \cdot 10^{-4} C^{-1} \cdot (-20\si{\degreeCelsius})} \cdot 10\si{\metre}^3 = \underline{\underline{8'552.23\si{\kg}}}
\end{align*}

\underline{Bei $35\si{\degreeCelsius}$:}
\begin{align*}
    \rho = \rho_0 \cdot \frac{1}{1 + \gamma \cdot \Delta\vartheta} \cdot 10^4\si{\litre} = 840\si{\kg/\metre}^3 \cdot \frac{1}{1 + 8.9 \cdot 10^{-4} C^{-1} \cdot 15\si{\degreeCelsius}} \cdot 10\si{\metre}^3 = \underline{\underline{8'289.34\si{\kg}}}
\end{align*}

\subsubsubsection{Die Dichteanomalie des Wassers}

Eine grosse Besonderheit -- auch Anomalie genannt -- des Wassers liegt darin, dass es bei $4\si{\degreeCelsius}$ seine grösste Dichte hat. Unterhalb dieser Temperatur dehnt es sich wieder aus, bis es bei 
    $0\si{\degreeCelsius}$ zu Eis erstarrt. \\\hspace*{\fill}

Eine zweite Besonderheit des Wassers liegt darin, dass es im erstarrten Zustand eine geringere Dichte hat als im flüssigen Zustand. Dies hat einerseits zur Folge, dass Eis in Wasser schwimmt (Die Dichte von Eis bei 
    $0\si{\degreeCelsius}$ ist fast 10\% geringer als diejenige von Wasser derselben Temperatur. Etwa 10\% des Eisvolumens ragt über die Wasseroberfläche.); andererseits dehnt sich Wasser beim Erstarren stark aus. Dabei kann es grosse Kräfte ausüben –- mit Wasser gefüllte Gefässe können beim Gefrieren des Wassers platzen (\glqq Sprengwirkung\grqq{} des Eises).
    \newpage

\subsubsection{Volumenausdehnung der Gase}

Bei Erwärmung dehnen sich Gase wesentlich stärker aus als Flüssigkeiten. Bleibt der Druck \textit{p} eines Gases während einer Temperaturänderung konstant, so ist die Volumenänderungen $\Delta V$ eines Gases mit dem 
    Volumen \textit{V} proportional zur Temperaturänderung $\Delta\vartheta$ sowie zum thermischen Volumenausdehnungskoeffizienten $\gamma$:
    \begin{equation}
        V = V_0 + \Delta V = V_0 + V_0 \cdot \gamma \cdot \Delta\vartheta = V_0 \cdot (1 + \gamma \cdot \Delta\vartheta) \label{eq:Volumenausdehnung Gase}
    \end{equation}

Der Volumenausdehnungskoeffizient $\gamma_{Gas}$ bei konstantem Druck ist näherungsweise für alle Gase gleich und ist folgendermassen definiert:
    \begin{equation}
        \gamma = \frac{1}{273.15\si{\kelvin}} \label{eq:Gamma Volumenausdehnung Gase}
    \end{equation}

Die temperaturbedingte Volumenzunahme von Gasen ist –- wie bei Flüssigkeiten -– im wesentlichen darauf zurückzuführen, dass die Geschwindigkeit der Atome beziehungsweise Moleküle im Gas zunimmt. Dadurch beanspruchen 
    die Teilchen mehr Raum, das Volumen des Gases nimmt zu. \\\hspace*{\fill}

Theoretisch ist laut der Formel (\ref{eq:Volumenausdehnung Gase}) das Volumen eines beliebigen, idealen Gases bei einer Temperatur von $-273.15\si{\kelvin} = 0$ (siehe Abbildung \ref{fig:Volumen vs Temperatur bei gleichem Druck}). \hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Grid
            \draw[step=0.5cm,white,very thin] (-2.3,-1.9) grid (12.3,4.4); % Used to align graph in middle

            % Axis
            \draw[thick,<->] (1.5,-1) -- (8.5,-1) node[anchor=north, shift={(13mm,0)}] {$\textrm{Temperatur} \, [\si{\degreeCelsius}]$}; % x axis
            \draw[thick] (2,-0.9) -- (2,-1.1) node[anchor=north, shift={(0,0)}] {-300};
            \draw[thick] (3,-0.9) -- (3,-1.1) node[anchor=north, shift={(0,0)}] {-200};
            \draw[thick] (4,-0.9) -- (4,-1.1) node[anchor=north, shift={(0,0)}] {-100};
            \draw[thick] (5,-0.9) -- (5,-1.1) node[anchor=north, shift={(0,0)}] {0};
            \draw[thick] (6,-0.9) -- (6,-1.1) node[anchor=north, shift={(0,0)}] {100};
            \draw[thick] (7,-0.9) -- (7,-1.1) node[anchor=north, shift={(0,0)}] {200};
            \draw[thick] (8,-0.9) -- (8,-1.1) node[anchor=north, shift={(0,0)}] {300};
            \draw[thick,->] (5,-1) -- (5,3.5) node[anchor=south, shift={(0,0)}] {Volumen}; % y axis
            \draw[thick] (4.9,0) -- (5.1,0) node[anchor=west, shift={(0,0)}] {1};
            \draw[thick] (4.9,1) -- (5.1,1) node[anchor=west, shift={(0,0)}] {2};
            \draw[thick] (4.9,2) -- (5.1,2) node[anchor=west, shift={(0,0)}] {3};
            \draw[thick] (4.9,3) -- (5.1,3) node[anchor=west, shift={(0,0)}] {4};

            % Line
            \draw[very thick,MyLightBlue] (6,3.099) -- (2.2685,-1);
            \draw[MyGreen,very thick] (3,-0.198) .. controls (2.5,-0.65) .. (1.5,-0.75) node[anchor=south, shift={(-11mm,-2.5mm)}] {Reales Gas};

            % Points
            \draw[draw=none,fill=MyLightBlue] (6,3.099) circle (0.075) node[anchor=west, shift={(0,1.5mm)}, text=MyLightBlue] {4.099}; % 4.099
            \draw[draw=none,fill=MyLightBlue] (5,2) circle (0.075) node[anchor=east, shift={(-2mm,0)}, text=MyLightBlue] {$V_0$}; % V_0
            \draw[draw=none,fill=MyLightBlue] (4,0.901) circle (0.075) node[anchor=east, shift={(0mm,3.5mm)}, text=MyLightBlue] {1.901}; % 1.901
            \draw[draw=none,fill=MyLightBlue] (3,-0.198) circle (0.075) node[anchor=east, shift={(0mm,4mm)}, text=MyLightBlue] {0.802}; % 0.802

            % Text
            \node[color=MyLightBlue] at (6.7,2.7) {Ideales Gas};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Volumen im Bereich von $-200\protect\si{\degreeCelsius} \protect\leq \protect\vartheta \protect\leq 100\protect\si{\degreeCelsius}$.}
    \label{fig:Volumen vs Temperatur bei gleichem Druck}
\end{figure}

Das kann natürlich nicht sein, da die Gasmoleküle ein Eigenvolumen haben. Tatsächlich verlassen wir aber für tiefe Temperaturen den Gültigkeitsbereich des Gay-Lussac'schen Gesetzes, das streng nur für ideale Gase 
    (die per Definition kein Eigenvolumen haben) gilt. Dennoch hat man aus diesem Verhalten geschlossen, dass es keine tiefere Temperatur als $-273.15\si{\kelvin}$ geben kann, weshalb dieser Punkt als absoluter Nullpunkt bezeichnet wird. Er bildet den Nullpunkt der Kelvin-Skala.
    \newpage

\subsubsubsection{Das Gesetz von Gay-Lussac}

Das Gesetz von Gay-Lussac gibt den Zusammenhang zwischen dem Volumen \textit{V} und der Temperatur \textit{T} eines idealen Gases bei Konstanthaltung des Drucks \textit{p} und der Teilchenzahl \textit{N} an. Eine 
    solche Zustandsänderung der Gasmenge bei konstantem Druck \textit{p} nennt man \textbf{isobar}. Die Formel dafür lautet:
    \begin{equation}
        \frac{V_1}{V_2} = \frac{T_1}{T_2} \ \text{bzw.} \ \frac{V_1}{T_1} = \frac{V_2}{T_2} \label{eq:Gesetz Gay-Lussac}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} 20 Liter Luft werden von $0\si{\degreeCelsius}$ auf $10\si{\degreeCelsius}$ erwärmt. \\\hspace*{\fill}

\textit{Gesucht:} Wie ändert sich das Volumen? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \frac{V_1}{T_1} = \frac{V_2}{T_2} \ \Rightarrow \ V_2 &= \frac{V_1 \cdot T_2}{T_1} = \frac{0.02\si{\metre}^3 \cdot 283.15\si{\kelvin}}{273.15\si{\kelvin}} = 2.073 \cdot 10^{-2}\si{\metre}^3 \\
                                                 \Delta V &= V_2 - V_1 = 2.073 \cdot 10^{-2}\si{\metre}^3 - 0.02\si{\metre}^3 = \underline{\underline{7.32 \cdot 10^{-4}\si{\metre}^3}}
\end{align*}
    
\subparagraph{Beispiel 2}

\textit{Gegeben:} Eine bestimmte Menge Luft hat bei $20\si{\degreeCelsius}$ ein Volumen von vier Liter. \\\hspace*{\fill}

\textit{Gesucht:} Bei welcher Temperatur hat die Luft ein Volumen von acht Liter? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \frac{V_1}{V_2} = \frac{T_1}{T_2} \ \Rightarrow \ T_2 = \frac{T_1 \cdot V_2}{V_1} = \frac{293.15\si{\kelvin} \cdot 8\si{\litre}}{4\si{\litre}} = \underline{\underline{586.3\si{\kelvin}}}
\]