\clearpage % flushes out all floats
\section{Elektrizitätslehre}

Das Wort \glqq Elektrizität\grqq{} ist abgeleitet vom griechischen Wort \glqq Elektron\grqq{} für Bernstein, ein gelber Schmuckstein aus Harz. Schon im Altertum war bekannt, dass geriebener Bernstein die merkwürdige 
    Eigenschaft hat, Haare, Staub, Textilfasern und andere leichte Dinge anzuziehen. Unter einem Elektron versteht man heute ein elektrisch negativ geladenes Elementarteilchen, das 1897 vom englischen Physiker Joseph Thomson entdeckt wurde. Thomson erklärte den Aufbau der Atome damit erstmalig mit den elektrischen Eigenschaften der Materie.

\subsection{Elektrostatik}
\subsubsection{Die Elektrische Ladung}

Neben der Masse ist die elektrische Ladung eine grundlegende Eigenschaft der Materie. Um 1730 bemerkte der Wissenschaftler Charles du Fay, dass sich geladene Körper entweder anziehen oder abstossen. Er schloss 
    daraus, dass es zwei Arten von Elektrizität geben müsse. Ein mit Katzenfell geriebener Bernsteinstab (Harz) trägt eine andere Sorte von Elektrizität als ein mit einem Seidentuch geriebener Glasstab. Charles du Fay sprach daher von \glqq Harzelektrizität\grqq{} (positive) und \glqq Glaselektrizität\grqq{} (negative):
    \begin{itemize}
        \item Ladungen, die zur gleichen Sorte gehören, \textbf{stossen sich ab}.
        \item Ladungen, die zu verschiedenen Sorten gehören, \textbf{ziehen sich an}.
    \end{itemize}

\subsubsection{Leiter und Nichtleiter}

Normalerweise lassen sich Stoffe in sogenannte Leiter (z.B. Metalle) und Nichtleiter -- auch Isolatoren genannt -- (z.B. Gummi, Keramik) einteilen. Der Unterschied zwischen Leiter und Isolator liegt in den 
    Elektronen oder besser gesagt, wie sie gebunden sind. Bei einem Nichtleiter sind die Elektronen lokal an die Atomkerne gebunden, bei einem Leiter sind sie eben nicht lokal gebunden, sie sind also verschiebbar (Abbildung \ref{fig:Leiter und Nichtleiter}).

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}

            \newcommand\Electron[2]{
                \fill[ball color=MyLightBlue!60] (#1,#2) circle (0.1) node[anchor=center, shift={(0mm,0mm)}, text=MyBlack] {\tiny $-$};
            }

            \newcommand\MetalAtom[2]{
                \fill[ball color=MyRed!60] (#1,#2) circle (0.5) node[anchor=center, shift={(0mm,0mm)}, text=MyBlack] {$Me^+$};
            }

            \newcommand\MetalAtomNa[2]{
                \fill[ball color=MyGrey!30] (#1,#2) circle (0.4) node[anchor=center, shift={(0mm,0mm)}, text=MyBlack] {$Na$};
            }

            \newcommand\MetalAtomCl[2]{
                \fill[ball color=MyYellow!50] (#1,#2) circle (0.5) node[anchor=center, shift={(0mm,0mm)}, text=MyBlack] {$Cl$};
                \Electron{#1-0.75}{#2-0.15} % Left bottom
                \Electron{#1-0.75}{#2+0.15} % Left top

                \Electron{#1-0.15}{#2+0.75} % Top left
                \Electron{#1+0.15}{#2+0.75} % Top right

                \Electron{#1+0.75}{#2+0.15} % Right top
                \Electron{#1+0.75}{#2-0.15} % Right bottom

                \Electron{#1+0.15}{#2-0.75} % Bottom right
                \Electron{#1-0.15}{#2-0.75} % Bottom left
            }
            
            % Left side
            %% Metal atoms
            \foreach \x in {-1,1,3}
                \foreach \y in {2,0,-2}{
                    \MetalAtom{\x}{\y}
                }
                
            %% Top electron layer
            \foreach \x in {-1.75,0.25,2.25}
                \foreach \y in {2.25}{
                    \Electron{\x}{rnd*\y+0.5} % rnd gives values between 0 and 1
                }

            %% Middle electron layer
            \foreach \x in {-1.75,0.25,2.25}
                \foreach \y in {0.5}{
                    \Electron{\x}{rand*\y} % rand gives values between -1 and 1
                }

            %% Bottom electron layer
            \foreach \x in {-1.75,0.25,2.25}
                \foreach \y in {-2.25}{
                    \Electron{\x}{rnd*\y-0.5}
                }

            % Dividing line
            \draw[very thick,draw=MyGrey,fill=none] (5,2.5) -- (5,-2.5);

            % Right side
            %% Metal atoms (Cl)
            \MetalAtomCl{7}{0} % Left
                \foreach \y in {2,-2}{
                    \MetalAtomCl{9}{\y} % Middle
                }
            \MetalAtomCl{11}{0} % Right

            %% Metal atoms (Na)
            \foreach \x in {7,11}
                \foreach \y in {2,-2}{
                    \MetalAtomNa{\x}{\y} % Left and right
                }
            \MetalAtomNa{9}{0} % Middle

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Links: Elektronengas in einem metallischen Leiter. Rechts: Ionenbindung im Isolator Natriumchlorid.}
    \label{fig:Leiter und Nichtleiter}
\end{figure}

Der \textit{Faraday'sche Käfig} ist eine praktische Anwendung eines Leiters in der heutigen Zeit. Die Hüllen von Autos oder Flugzeugen bilden diese Faraday'schen Käfige, welche die Insassen weitgehend vor z.B. 
    Blitzschlägen schützen.
    \newpage

\subsubsection{Das Coulomb'sche Gesetz}

Bereits zu Beginn des 18. Jahrhunderts vermutete man, dass die elektrische Kraft zwischen zwei punktförmigen Ladungen mit dem Quadrat der Entfernung abnimmt, wie dies auch der Fall für die Gravitationskraft ist. 
    Charles Coulomb gelang es 1784 diese Vermutung mit einem Experiment zu beweisen, weswegen die Kräfte $Q \ [\si{\coulomb}]$ zwischen Ladungen mit dem \textit{Coulomb'schen Gesetz} angegeben werden:
    \begin{equation}
        F_c = k \cdot \frac{Q_1 \cdot Q_2}{r^2} \ [\si{\newton}] \label{eq:Coulombsche Gesetz}
    \end{equation}

\textit{k} ist eine Konstante mit dem Wert $k = 8.988 \cdot 10^9\frac{\si{\newton} \cdot \si{\metre}^2}{\si{\coulomb}^2}$ und wird mit der Formel $k = \frac{1}{4 \cdot \pi \cdot \epsilon_0}$ berechnet, wobei 
    $\epsilon_0$ die elektrische Feldkonstante mit dem Wert $\epsilon_0 = 8.854 \cdot 10^{-12}\frac{\si{\coulomb}^2}{\si{\newton} \cdot \si{\metre}^2}$ ist.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}
            \fill[ball color=MyRed!60] (-2,0) circle (5mm);
            \fill[ball color=MyRed!60] (2,0) circle (5mm);
            \draw[Latex-,very thick] (-3.5,0) -- (-2.5,0);
            \node at (-2,0) {\Huge{$+$}};
            \node at (2,0) {\Huge{$+$}};
            \draw[-Latex,very thick] (2.5,0) -- (3.5,0);
    
    
            \draw[dashed] (-2,-0.5) -- (-2,-1.5);
            \draw[dashed] (2,-0.5) -- (2,-1.5);
            \draw[Latex-Latex] (-2,-1) -- (2,-1);
            \node at (0,-0.75) {$r$};
    
            \node at (-2.75,0.3) {$\overrightarrow{F}_1$};
            \node at (2.75,0.3) {$\overrightarrow{F}_2$};
    
            \fill[ball color=MyRed!60] (-2,-2) circle (5mm);
            \fill[ball color=MyDarkBlue!60] (2,-2) circle (5mm);
            \draw[-Latex,very thick] (-1.5,-2) -- (-0.5,-2);
            \node at (-2,-2) {\Huge{$+$}};
            \node at (2,-2) {\Huge{$-$}};
            \draw[Latex-,very thick] (0.5,-2) -- (1.5,-2);
    
            \node at (-1.25,-1.7) {$\overrightarrow{F}_1$};
            \node at (1.25,-1.7) {$\overrightarrow{F}_2$};
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Grafische Darstellung der Coulomb-Kraft.}
\end{figure}

Wenn man als Beispiel zwei punktförmige Ladungsträger nimmt, die je eine Ladung von einem Coulomb haben, und ein Meter voneinander entfernt sind, so üben sie eine Kraft von $9 \cdot 10^9\si{\newton}$ aus.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Zwei kleine Kugeln mit einer Masse von je einem Kilogramm haben genau einen Meter Abstand. \\\hspace*{\fill}

\textit{Gesucht:} Wie stark müssen sie geladen sein, damit die Coulomb-Kraft gleich $\overrightarrow{F}_G$ ist? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \overrightarrow{F}_G = \overrightarrow{F}_C \ \Rightarrow \ m \cdot g = k \cdot \frac{Q_1 \cdot Q_2}{r^2} \ \Rightarrow \ Q^2 &= \frac{m \cdot g \cdot r^2}{k} \\
                                                                                                                              Q &= \sqrt{\frac{1\si{\kg} \cdot 9.81\si{\metre/\second}^2 \cdot (1\si{\metre})^2}{8.988 \cdot 10^9\frac{\si{\newton} \cdot \si{\metre}^2}{\si{\coulomb}^2}}} \\
                                                                                                                                &= \underline{\underline{3.3 \cdot 10^{-5}\si{\coulomb}}}
\end{align*}
\newpage

\subsubsection{Das elektrische Feld}

1687 erklärte Newton mit dem Gravitationsgesetz die Planetenbewegung. Newton dachte dabei an eine Fernwirkung, welche sich im Raum instantan von einem Himmelskörper zum anderen überträgt. Das gleiche Problem stellt 
    sich mit elektrisch geladenen Körpern, die sich gemäss dem Coulomb'schen Gesetz gegenseitig beeinflussen, ohne einander zu berühren. Einen Erklärungsansatz brachten die von Michael Faraday eingeführten \textit{Feld- oder Kraftlinien}. Gegenüber der Newton'schen Vorstellung einer Fernwirkung von Kräften im Raum setzte Faraday mit seinen Feldlinien als Erster und gegen den Widerstand von Zeitgenossen eine sehr erfolgreiche Nahwirkungstheorie der Übertragung von Kräften durch. Diese Theorie geht von einer Wirkung geladener Teilchen mit dem Feld aus. Veränderungen in diesem Feld breiten sich nicht instantan, sondern mit einer endlichen Geschwindigkeit aus. \\\hspace*{\fill}

Ein physikalisches Feld existiert im Raum, der leer oder auch stoffgefüllt sein kann, und drückt eine messbare physikalische Eigenschaft aus, die jedem Raumpunkt zugeordnet werden kann. Die physikalischen Grössen 
    können \textit{Skalare} wie die Temperatur oder \textit{Vektoren} wie die Windgeschwindigkeit sein. Die \textbf{Definition eines elektrischen Feldes ist ein Raum, in dem (ruhende) Ladungen eine Kraft erfahren}. Diese Definition führt zur \textbf{elektrischen Feldstärke \textit{E}}, die man folgendermassen definieren kann:
    \begin{equation}
        E = \frac{F_C}{q} \ \left[\frac{\si{\newton}}{\si{\coulomb}}\right] \label{eq:Elektrische Feldstärke E}
    \end{equation}

\textit{q} wird als sogenannte Testladung deklariert.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine freistehende Metallkugel hat eine Ladung von $1.2 \cdot 10^{-6}\si{\coulomb}$. \\\hspace*{\fill}

\textit{Gesucht:} Berechnen Sie die Feldstärke im Abstand von $0.5\si{\metre}$ vom Kugelzentrum. \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    E = \frac{F_C}{q} \ \Rightarrow \ E &= \frac{k \cdot \frac{Q_1 \cdot \cancel{Q_2}}{r^2}}{\cancel{q}} = k \cdot \frac{Q}{r^2} \\
                                        &= 8.988 \cdot 10^9\frac{\si{\newton} \cdot \si{\metre}^2}{\si{\coulomb}} \cdot \frac{1.2 \cdot 10^{-6}\si{\coulomb}}{(0.5\si{\metre})^2} = \underline{\underline{43'142.4\frac{\si{\newton}}{\si{\coulomb}}}}
\end{align*}
\newpage

\subsubsection{Elektrische Feldlinien}

Elektrische Felder können anschaulich durch elektrische Feldlinien dargestellt werden. Darunter versteht man eine Linie, deren Richtung in jedem Punkt eines elektrischen Feldes mit der Richtung der Kraft auf einen 
    positiv geladenen Körper übereinstimmt.

\begin{figure}[H]    
    \begin{center}
        \begin{tikzpicture}
            \foreach \x in {0,5,...,360}{
                \draw[-Latex,MyRed!30,rotate=\x] (0,0) -- (3,0);
                \draw[-Latex,MyRed!30,rotate=\x] (0,0) -- (2.5,0);
                \draw[-Latex,MyRed!30,rotate=\x] (0,0) -- (2,0);
                \draw[-Latex,MyRed!30,rotate=\x] (0,0) -- (1.5,0);
            }
            
            \fill[ball color=MyRed!60] (0,0) circle (5mm);
            \node at (0,0) {\Huge{$+$}};
            \node at (0.75,-0.25) {$q_1$};
            \fill[ball color=MyDarkBlue!60,rotate=45] (2,0) circle (2mm);
            \draw[-Latex,MyRed,rotate=45,very thick] (2,0) -- (3,0);
            \draw[Latex-,MyDarkBlue,rotate=45,very thick] (1,0) -- (2,0);
            \node at (1.8,1.3) {$q_2$};
            \node at (1.2,2) {$\overrightarrow{E}(x,y)$};
            \node at (0.5,1.3) {$\overrightarrow{F}(x,y)$};
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Testladung \protect{$q_2$} im elektrischen Feld einer Ladung \protect{$q_1$}.}
\end{figure}

Wegen der Eindeutigkeit der Kraftrichtung kann in jedem Punkt eines elektrischen Feldes genau eine Feldlinie gezeichnet werden. Beim Zeichnen von Feldlinien müssen ein paar Regeln beachtet werden:
    \begin{itemize}
        \item Feldlinien verlaufen stets von der positiven zur negativen Ladung.
        \item Feldlinien schneiden sich nie.
        \item Feldlinien werden dort dicht gezeichnet, wo die Feldstärke gross ist.
        \item Feldlinien des elektrostatischen Feldes stehen senkrecht auf der Leiteroberfläche.
    \end{itemize}

\subsubsection{Elektronen im homogenen elektrischen Feld}

Ein freies Elektron befindet sich an der negativen Platte eines Kondensators. Durch die Feldstärke erfährt das Elektron wegen seiner negativen Ladung eine Beschleunigung parallel zu einer Feldlinie in Richtung der 
    positiven Platte.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            \newcommand\Electron[2]{
                \fill[ball color=MyLightBlue!60] (#1,#2) circle (0.1) node[anchor=center, shift={(0mm,0mm)}, text=MyBlack] {\tiny $-$};
            }

            % Left wall
            \draw[thick,draw=MyGrey,fill=MyLightGrey] (2,3) -- (2.25,3) -- (2.25,-2) -- (2,-2) -- cycle node[anchor=center, shift={(1.25mm,2.5mm)}, text=MyRed] {\large $+Q$};

            % Right wall
            \draw[thick,draw=MyGrey,fill=MyLightGrey] (7.75,3) -- (8,3) -- (8,-2) -- (7.75,-2) -- cycle node[anchor=center, shift={(1.25mm,2.5mm)}, text=MyDarkBlue] {\large $-Q$};

            % Arrows
            \draw[<->,thick,MyBlack] (2.25,2) -- (7.75,2) node[anchor=south, shift={(-27.5mm,0mm)}, text=MyBlack] {$\Delta s$}; % Delta s
            \draw[->,thick,MyBlack] (2.25,-1) -- (5,-1) node[anchor=south, shift={(-13.75mm,0mm)}, text=MyBlack] {$\overrightarrow{E}$}; % E
            \draw[->,thick,MyBlack] (6.5,0.5) -- (3.75,0.5) node[anchor=south, shift={(13.75mm,0mm)}, text=MyBlack] {$\overrightarrow{F}$}; % Force on electron

            % Electron
            \Electron{6.5}{0.5}

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Kraft auf einem Elektron im Inneren eines Plattenkondensators.}
\end{figure}
\newpage

Wenn wir uns nun überlegen, welche Kraft auf das Elektron wirkt, dann kann man einerseits die Formel für die elektrische Feldstärke (\ref{eq:Elektrische Feldstärke E}) umformen und $F_C = E \cdot q$ erhalten. 
    Andererseits kann die grundlegende Formel $\overrightarrow{F} = m \cdot a$ verwendet werden. \\\hspace*{\fill}

Will man nun die Beschleunigung des Electrons wissen, löst man die Formeln nach der Beschleunigung \textit{a} auf:
    \begin{equation*}
        m \cdot a = \overrightarrow{E} \cdot q \ \Rightarrow \ a = \frac{Q \cdot \overrightarrow{E}}{m}
    \end{equation*}
    wobei man die Feldstärke \textit{E} mit $E = \frac{U}{\Delta s}$ substituieren kann. Somit erhält man den Ausdruck:
    \begin{equation}
        a = \frac{Q}{m} \cdot \frac{U}{\Delta s} \label{eq:Beschleunigung Elektron}
    \end{equation}

In diesem Beispiel entspricht die Ladung \textit{Q} genau der Ladung des Elektrons und seiner Masse, was der spezifischen Ladung des Elektrons von $1.7588 \cdot 10^{11}\frac{\si{\joule}}{\si{\kg}}$ gleichkommt.