\clearpage % flushes out all floats
\section{Arbeit, Energie, Leistung}

Wirkt eine Kraft auf einen Körper ein und bewirkt dabei eine Verformung, eine Beschleunigung oder ein Anheben des Körpers, so wird physikalische Arbeit verrichtet. Um die Grösse der verrichteten Arbeit zu bestimmen, 
    müssen der Betrag der Kraft $\overrightarrow{F}$ und die Länge des Weges \textit{s}, entlang dessen die Kraft wirkt, bekannt sein. Unter der Bedingung, dass die Kraft konstant ist und in beliebiger, aber fester Richtung wirkt, gilt:
    \begin{equation}
        W = \overrightarrow{F} \cdot s \cdot \cos(\alpha) \ [\si{\newton\metre}] = \left[\frac{\si{\kilo\gram} \cdot \si{\metre}^2}{\si{\second}^2}\right] = [\si{\joule}] \label{eq:Arbeit}
    \end{equation}

Hierbei bezeichnet $\alpha$ den Winkel zwischen der wirkenden Kraft und der zurückgelegten Wegstrecke. \hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            % Ground
            \draw[color=MyBlack,thick] (3,0) -- (7,0);
            \foreach \x in {0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.25,2.5,2.75,3,3.25,3.5,3.75}
                \draw[color=MyBlack,thick] (\x + 3,0) -- (\x + 2.75,-0.25);
    
            % Sledge
            \draw[color=MyBrown,thick] (3.5,0.025) -- (5,0.025); % Bottom beam
            \draw[color=MyBrown,thick] (3.75,0.025) -- (3.75,0.5); % Left leg
            \draw[color=MyBrown,thick] (3.5,0.5) -- (5,0.5); % Top beam
            \draw[color=MyBrown,thick] (4.75,0.025) -- (4.75,0.5); % Right leg
            \draw[color=MyBrown,thick] (5,0.025) arc (270:360:0.25); % Curved bit
    
            % Triangle
            \draw[color=MyBlack,thick] (5.25,0.275) -- (6.5,0.275);
            \draw[color=MyLightBlue,thick,->] (5.25,0.275) -- (6.5,1) node[anchor=west, shift={(0mm,2.5mm)}] {$\overrightarrow{F}$};
            \draw[thick] (6,0.275) arc (0:30.964:0.75) node[anchor=east, shift={(0.5mm,-2.5mm)}] {\small $\alpha$};
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Arbeit wird verrichtet, wenn man einen Schlitten zieht.}
\end{figure}

Eine wichtige Umrechnung, die einem bekannt sein sollte, ist die Umwandlung von Kilowattstunden [$\si{\kilo\watt\hour}$] zu Joule [$\si{\joule}$]. Sie lautet folgendermassen:
    \begin{equation}
        1\si{\kilo\watt\hour} = 3.6 \cdot 10^6\si{\joule} \label{eq:kWh zu Joule}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine Masse von 4 Kilogramm wird auf 15 Meter Höhe mit $5\si{\centi\metre/\second}^2$ bewegt. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Welche Arbeit wird dabei verrichtet? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}

\[
    W = \overrightarrow{F} \cdot s = m \cdot a \cdot s = 4 \cdot 0.05\si{\metre/\second}^2 \cdot 15\si{\metre} = \underline{\underline{3\si{\joule}}}
\]
\newpage

\subsection{Potenzielle Energie/Hubarbeit}

Erfahrungsgemäss ist es einfacher, einen leichten Körper hoch zu heben als einen schweren. Doch auch beim Heben zweier gleich schwerer Körper gibt es Unterschiede: Je weiter man einen Körper hoch heben muss, desto 
    mehr Arbeit ist dafür nötig. Die Hubarbeit $W_{Hub}$ ist proportional zur Gewichtskraft $\overrightarrow{F}_G$ eines angehobenen Körpers und zur Hubhöhe \textit{h}:
    \begin{equation}
        W_{Hub} = \overrightarrow{F}_G \cdot h = m \cdot g \cdot h \label{eq:Hubarbeit}
    \end{equation}

Nachdem der Körper um die Höhe \textit{h} angehoben worden ist, hat er bezüglich seiner ursprünglichen Lage eine \textit{potenzielle Energie}:
    \begin{equation}
        E_{Pot} = m \cdot g \cdot h \label{eq:Potenzielle Energie}
    \end{equation}

Wichtig ist, dass man die Goldene Regel der Mechanik im Hinterkopf hat. Sie besagt, dass was an Kraft eingespart wird, durch einen längeren Weg ausgeglichen werden muss. Dies lässt sich an einem Flaschenzug gut 
    beobachten: Je mehr Rollen vorhanden sind, desto weniger Kraft $\overrightarrow{F}$ muss eingesetzt werden, aber desto länger ist das Seil, um einen Körper dieselbe Höhe zu heben.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Gewichtheber reisst 175 Kilogramm auf 2.1 Meter und hält das Gewicht für drei Sekunden. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:}
\begin{enumerate}
    \item Wo wird Arbeit geleistet, wo nicht?
    \item Wie gross ist die geleistete Arbeit?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item Beim Hochheben wird Arbeit geleistet, beim Obenhalten nicht.
    \item $W_{Hub} = m \cdot g \cdot h = 175\si{\kilo\gram} \cdot 9.81\si{\metre/\second}^2 \cdot 2.1\si{\metre} = \underline{\underline{3'605.18\si{\joule}}}$
\end{enumerate}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Eine Aufzugskabine hat eine Masse 1'200 Kilogramm. Die Masse des Gegengewichtes ist 1'100 Kilogramm. Eine Person in der Kabine ist 75 Kilogramm schwer. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Wie gross ist die Arbeit, um die Person 50 Meter nach oben zu befördern? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}

\[
    W_{Hub} = m \cdot g \cdot h = ((1'200\si{\kilo\gram} - 1'100\si{\kilo\gram}) + 75\si{\kilo\gram}) \cdot 9.81\si{\metre/\second}^2 \cdot 50\si{\metre} = \underline{\underline{85'837.5\si{\joule}}}
\]
\newpage

\subsection{Beschleunigungsarbeit/Kinetische Energie}

Um einen Körper mit der Masse \textit{m} und der Anfangsgeschwindigkeit $v_0$ geradlinig gleichförmig auf eine Endgeschwindigkeit \textit{v} zu beschleunigen, ist eine Beschleunigungsarbeit erforderlich:
    \begin{equation}
        W_{Beschl} = \overrightarrow{F} \cdot s = m \cdot a \cdot s
    \end{equation}


Aus der Kinematik der geradlinig gleichförmig beschleunigten Bewegung kennen wir den Ausdruck (siehe Formel (\ref{eq:2_Geschw. gleichm. beschl. Beweg. mit Anfangsgeschw.}))
    \begin{equation}
        v^2 = v_0^2 + 2as \ \textrm{bzw.} \ a \cdot s = \frac{1}{2} \cdot (v^2 - v_0^2) \nonumber
    \end{equation}

und berechnen damit die Beschleunigungsarbeit:
    \begin{equation}
        W_{Beschl} = \overrightarrow{F} \cdot s = m \cdot a \cdot s = \frac{1}{2} \cdot m \cdot (v^2 - v_0^2) \label{eq:Beschleunigungsarbeit}
    \end{equation}

Hat der Körper seine Endgeschwindigkeit \textit{v} erreicht, so besitzt er \textit{kinetische Energie}. \\\hspace*{\fill}

Falls der Körper \textbf{keine Anfangsgeschwindigkeit} $v_0$ besitzt, kann die Formel (\ref{eq:Beschleunigungsarbeit}) vereinfacht werden:
    \begin{equation}
        W_{Beschl} = E_{Kin} = \frac{1}{2} \cdot m \cdot v^2 \label{eq:Beschleunigungsarbeit v0 = 0}
    \end{equation}

Hatte der Körper schon \textbf{zu Beginn eine Geschwindigkeit} $v_0$, so besass er eine kinetische Anfangsenergie $E_{Kin,0} = \frac{1}{2} \cdot m \cdot v_0^2$. Für den Beschleunigungsvorgang ist eine 
    Beschleunigungsarbeit erforderlich, die gleich der Differenz von End- und Anfangsenergie ist:
    \begin{equation}
        W_{Beschl} = E_{Kin} - E_{Kin,0} = \frac{1}{2} \cdot m \cdot (v^2 - v_0^2) \label{eq:Beschleunigungsarbeit v0 ungleich 0}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein PKW ist 1'200 Kilogramm schwer. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Bei welcher Geschwindigkeit hat er die Bewegungsenergie von $1\si{\mega\joule}$? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}

\[
    1 \cdot 10^6\si{\joule} = \frac{1}{2}mv^2 \ \Rightarrow \ v = \sqrt{\frac{1 \cdot 10^6\si{\joule}}{\frac{1}{2}m}} = \sqrt{\frac{1 \cdot 10^6\si{\joule}}{\frac{1}{2} \cdot 1'200\si{\kilo\gram}}} = \underline{\underline{40.82\si{\metre/\second}}}
\]
\newpage

\subsection{Spannarbeit/Spannenergie}

Beim Spannen einer Feder ist die Kraft $\overrightarrow{F}$ längs des Spannwegs \textit{y} nicht konstant, sondern nimmt gemäss dem Federgesetz $\overrightarrow{F} = D \cdot y$ (Formel (\ref{eq:Hook'sche Federgesetz})) 
    proportional zur Verlängerung \textit{y} zu. \\\hspace*{\fill}

Weil die Kraft nicht konstant ist, kann die Arbeit nicht mehr einfach als Produkt aus Kraft und Weg berechnet werden. Durch Unterteilen des Spannwegs in kleine Wegintervalle lässt sich leicht zeigen, dass die 
    Spannarbeit auch in diesem Fall als Fläche unter dem \textit{F-y-Graphen} (Abbildung \ref{Arbeitsdiagramm der Spannarbeit}) -- hier eine Dreiecksfläche -- berechnet werden kann. \hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            % Shaded area
            \draw[fill=MyGrey!25,draw=none] (3.5,-1) -- (6,-1) -- (6,1.5) -- cycle;
    
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,-1) -- (6.5,-1) node[anchor=north, shift={(0,0)}] {$y$}; % x axis
            \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$\overrightarrow{F}$}; % y axis
            \draw[thick] (3.5,-0.75) -- (3.5,-1) -- (3.75,-1); % make axes look connected
            
            % Line
            \draw[MyLightBlue,very thick] (3.5,-1) -- (6,1.5);
            \draw[MyBlack,thick,dashed] (6,-1) -- (6,1.5);
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Arbeitsdiagramm der Spannarbeit.}
    \label{Arbeitsdiagramm der Spannarbeit}
\end{figure}

Die Spannarbeit kann mit der folgenden Formel berechnet werden, wobei \textit{D} die Federkonstante des Körpers angibt:
    \begin{equation}
        W_{Spann} = E_{Spann} = \overrightarrow{F} \cdot s = \frac{1}{2} \cdot D \cdot y^2 \label{eq:Spannarbeit bzw. Spannenergie}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Die zwei Pufferfedern eines Eisenbahnwagens ($m = 10\si{\tonne}$) werden um 10 Zentimeter zusammengedrückt, wenn er mit $7.2\si{\kilo\metre/\hour}$ auf ein Hinderniss prallt. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Wie gross ist die Federkonstante D? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}
\begin{align*}
    W = \frac{1}{2} \cdot D \cdot y^2 \ \Rightarrow \ D = \frac{W}{\frac{1}{2} \cdot y^2} &= \frac{\frac{1}{2}m \cdot v^2}{\frac{1}{2} \cdot y^2} = \frac{\frac{1}{2} \cdot 10'000\si{\kilo\gram} \cdot (2\si{\metre/\second})^2}{\frac{1}{2} \cdot 0.1\si{\metre}^2}\\
                                                                                          &= 4'000'000 \text{ für beide Federn, } \underline{\underline{2'000'000\text{ pro Feder}}}
\end{align*}
\newpage

\subsection{Die Leistung}

Die Leistung ist v.a. in der Technik eine wichtige Grösse, die den Energieverbrauch pro Zeiteinheit eines Geräts oder einer Maschine angibt. Wichtig ist, dass möglichst energieeffiziente Geräte und Maschinen 
    eingesetzt werden, welche die (zugeführte) Energie mit möglichst kleinem Verlust umsetzen. \\\hspace*{\fill}

Unter der physikalischen Leistung \textit{P} versteht man den Quotienten aus der verrichteten Arbeit $\Delta W$ und der dazu erforderlichen Zeit $\Delta t$:
    \begin{equation}
        P = \frac{\Delta W}{\Delta t} \ [\si{\watt}] = \left[\frac{\si{\joule}}{\si{\second}}\right] \label{eq:Leistung}
    \end{equation}

Obwohl die Pferdestärke (PS) seit 30 Jahren keine gesetzliche Einheit mehr ist, hat sie sich bei Fahrzeugen im allgemeinen Sprachgebrauch gehalten. Es gilt folgender Zusammenhang:
    \begin{equation}
        1\textrm{PS} = 735.5\si{\watt} \label{eq:PS zu Watt}
    \end{equation}

In der Elektrowirtschaft hingegen wird der Energiebezug meist über die Leistung in der Einheit Kilowattstunde ($\si{\kilo\watt\hour}$) verrechnet. Es gilt:
    \begin{equation}
        1\si{\kilo\watt\hour} = 1'000\si{\watt} \cdot 3'600\si{\second} = 3.6 \cdot 10^6\si{\watt\second} = 3.6 \cdot 10^6\si{\joule} = 3.6\si{\mega\joule}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein PKW hat die Masse von $1'200\si{\kilo\gram}$. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:}
\begin{enumerate}
    \item Welches P ist erforderlich, um den PKW in sechs Sekunden aus dem Stand auf $108\si{\kilo\metre/\hour}$ zu beschleunigen?
    \item In welcher Zeit beschleunigt der PKW, welches eine Leistung von $150\si{\kilo\watt}$ hat, aus der Ruhe auf $108\si{\kilo\metre/\hour}$?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item $P = \frac{\Delta W}{\Delta t} = \frac{\frac{1}{2}m \cdot \left(v^2 - v_0^2\right)}{\Delta t} = \frac{\frac{1}{2} \cdot 1'200\si{\kilo\gram} \cdot \left((30\si{\metre/\second})^2 - (0\si{\metre/\second})^2\right)}{6\si{\second}} = \underline{\underline{90'000\si{\watt}}}$
    \item $P = \frac{\Delta W}{\Delta t} \ \Rightarrow \ \Delta t = \frac{\Delta W}{P} = \frac{\frac{1}{2}m \cdot \left(v^2 - v_0^2\right)}{P} = \frac{\frac{1}{2} \cdot 1'200\si{\kilo\gram} \cdot \left((30\si{\metre/\second})^2 - (0\si{\metre/\second})^2\right)}{150'000\si{\watt}} = \underline{\underline{3.6\si{\second}}}$
\end{enumerate}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Auto wiegt $9\si{\kilo\newton}$, sein Motor leistet $45\si{\kilo\watt}$. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} In welcher Zeit wird das Auto den Höhenunterschied von $1'500\si{\metre}$ überwinden? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}

\[
    P = \frac{\Delta W}{\Delta t} \ \Rightarrow \ \Delta t = \frac{\Delta W}{P} = \frac{mgh}{P} = \frac{9'000\si{\newton} \cdot 1'500\si{\metre}}{45'000\si{\watt}} = \underline{\underline{300\si{\second}}}
\]

\subparagraph{Beispiel 3}

\textit{Gegeben:} Ein Sportboot hat eine Leistung von $75\si{\kilo\watt}$ bei konstanter Geschwindigkeit von $12\si{\metre/\second}$. Um sich mit derselben Geschwindigkeit einen Wasserskifahrer zu ziehen, muss $83\si{\kilo\watt}$ aufgewendet werden. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Welche Kraft wird gebraucht, um den Wasserskifahrer zu ziehen? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}
\begin{align*}
    P &= \frac{\Delta W}{\Delta t} = \frac{\overrightarrow{F} \cdot s}{\Delta t} = \overrightarrow{F} \cdot \overbrace{v}^{\frac{s}{t} = v} \\
    \rightarrow \overrightarrow{F}_1 &= \frac{P_1}{v} = \frac{75'000\si{\watt}}{12\si{\metre/\second}} = 6'250\si{\newton} \\    
    \rightarrow \overrightarrow{F}_2 &= \frac{P_2}{v} = \frac{83'000\si{\watt}}{12\si{\metre/\second}} = 6'917\si{\newton} \\
    \Rightarrow \Delta \overrightarrow{F} &= \overrightarrow{F}_1 - \overrightarrow{F}_2 = \underline{\underline{666.\overline{6}\si{\newton}}}
\end{align*}

\subsection{Der Wirkungsgrad}

Sofern bei einem mechanischen Prozess die stets auftretende Reibung sehr klein gehalten werden kann, so ist es möglich, sie bei der Formulierung von physikalischen Gesetzen unberücksichtigt zu lassen. Bei genauer 
    Betrachtung zeigt sich allerdings, dass die von einer mechanischen Einrichtung aufgenommene Arbeit stets grösser ist als die von ihr abgegebene Arbeit. Es scheint also Arbeit verloren gegangen zu sein, der Satz von der Erhaltung der mechanischen Arbeit scheint somit seine Geltung zu verlieren. \\\hspace*{\fill}

Tatsächlich verschwindet die verrichtete Arbeit allerdings nicht, sondern es wird stets eine entsprechend grosse Menge an Reibungsarbeit verrichtet. Das Gesetz von der Erhaltung der mechanischen Arbeit kann -– unter 
    Berücksichtigung der Reibung –- somit folgendermassen formuliert werden:
    \begin{center}
        Zugeführte Leistung = Abgegebene Leistung + Reibungsarbeit
    \end{center}

Der Wirkungsgrad $\eta$ (eta) beschreibt dieses Verhältnis zwischen abgegebener Nutzleistung $P_{ab}$ und zugeführter Leistung $P_{zu}$ und beschreibt die Effizienz einer Maschine. Er wird folgendermassen berechnet:
    \begin{equation}
        \eta = \frac{P_{ab}}{P_{zu}} \label{eq:Wirkungsgrad}
    \end{equation}

Der Wirkungsgrad ist eine dimensionslose Grösse, hat also keine Einheit und einen Wert zwischen 0 und 1. Da sich Reibung oder andere Energieverlustquellen niemals komplett beseitigen lassen, gibt es keine 
    mechanische Einrichtung mit einem Wirkungsgrad von $\eta = 1$. Daher gilt immer $0 \leq \eta \leq 1$.
    \newpage

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine Turbine liegt 15 Meter unter der Wasseroberfläche. Pro Sekunde fliessen $3.5\si{\metre}^2$ Wasser hindurch. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Welche Leistung gibt die Turbine ab, wenn der Wirkungsgrad bei 90\% liegt? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}
\begin{align*}
    \text{Potenzielle Energie: }& \overrightarrow{F} = mgh = \frac{\overbrace{3'500\si{\kilo\gram}}^{3.5\si{\metre}^2 \text{ = } 3'500\si{\kilo\gram}} \cdot 9.81\si{\metre/\second}^2 \cdot 15\si{\metre}}{1\si{\second}} = 515'025\si{\watt} \\
    \text{Turbinenleistung: }& 515'025\si{\watt} \cdot 0.9 = \underline{\underline{463'522.5\si{\watt}}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Förderband befördert pro Stunde 130 Tonnen Kohle alle 2.8 Meter, die gebohren werden. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:}
\begin{enumerate}
    \item Wie gross ist die Arbeit?
    \item Wie gross ist der Wirkungsgrad in Prozent, wenn der Arbeitsmotor $1.3\si{\kilo\watt}$ abgibt (was er aus dem Stecker zieht)?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item $\text{Hubarbeit: } P^{ab} = \frac{\Delta W}{\Delta t} = \frac{mgh}{3'600\si{\second}} = \frac{130'000\si{\kilo\gram} \cdot 9.81\si{\metre/\second}^2 \cdot 2.8\si{\metre}}{3'600\si{\second}} = \underline{\underline{991.9\si{\watt}}}$
    \item $\eta = \frac{991.9\si{\watt}}{1'300\si{\watt}} = 0.763 = \underline{\underline{76.3\%}}$
\end{enumerate}

\subparagraph{Beispiel 3}

\textit{Gegeben:} Einem Automotor werden pro Stunde 6.8 Liter Benzin zugeführt. Er gibt eine Leistung von $45\si{\kilo\watt}$ ab. Der Energiegehalt von einem Liter Benzin beträgt $43'000\si{\kilo\joule/\kilo\gram}$, die Dichte beträgt $0.7\si{\kilo\gram/\deci\metre}^3$. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Wie hoch ist der Wirkungsgrad des Autos in Prozent? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}
\begin{align*}
    m &= \rho \cdot V = 700\si{\kilo\gram/\deci\metre}^3 \cdot 6.8 \cdot 10^{-3} = 4.76\si{\kilo\gram} \\
    P^{zu} &= \frac{\Delta W}{\Delta t} = \frac{E_{Benzin}}{\Delta t} = \frac{4.76\si{\kilo\gram} \cdot 43'000'000\si{\joule/\kilo\gram}}{3'600\si{\second}} = 56'855.\overline{5}\si{\watt} \\
    \eta &= \frac{45'000\si{\watt}}{56'855.\overline{5}\si{\watt}} = 0.791 = \underline{\underline{79.1\%}}
\end{align*}
\newpage

\subsection{Der Energieerhaltungssatz in der Mechanik}

Ein (ideales) abgeschlossenes System ist ein energetisch isoliertes System, das weder Energie gegen aussen abgeben noch von aussen aufnehmen kann. Die Gesamtenergie des Systems bleibt erhalten und somit konstant. Deshalb kann Energie weder erzeugt noch vernichtet, sondern stets nur von einer Form in eine andere umgewandelt werden. Die Allgemeine Formel des Energiesatzes lautet wie folgt:
\begin{equation}
    E_{Tot} = E_{Kin} + E_{Pot} \label{Energieerhaltung allgemein}
\end{equation}

Folgende Formeln und die Abbildung \ref{Energieerhaltung eines Körpers} illustrieren diese Eigenschaft etwas näher:
\begin{align*}
    E_{Tot,Punkt \ A} &= E_{Pot,Punkt \ A} + E_{Kin,Punkt \ A} = E_{Pot,Punkt \ B} + E_{Kin,Punkt \ B} \ \ldots \\
    E_{Tot,Punkt \ A} &= E_{Tot,Punkt \ B} \ \ldots
\end{align*}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            % Ground
            \draw[color=MyBlack,thick] (3,-0.5) -- (7,-0.5);
            \foreach \x in {0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.25,2.5,2.75,3,3.25,3.5,3.75}
                \draw[color=MyBlack,thick] (\x + 3,-0.5) -- (\x + 2.75,-0.75);
    
            % Lines
            \draw[color=MyBlack,thick] (3.5,-0.5) -- (3.5,2) node[pos=0.5,anchor=east, shift={(0mm,0cm)}] {$h_A$}; % Left most line
            \draw[color=MyBlack,thick,dashed] (3.5,2) -- (4.5,2); % Top most dashed line
           
            \draw[color=MyBlack,thick] (4.5,-0.5) -- (4.5,0.75) node[pos=0.5,anchor=east, shift={(0mm,0cm)}] {$h_B$}; % Middle black line
            \draw[color=MyBlack,thick,dashed] (4.5,0.75) -- (5.5,0.75); % Middle dashed line
            \draw[color=MyRed,thick] (4.5,0.75) -- (4.5,2) node[pos=0.5,anchor=west, shift={(0mm,0cm)}] {$s = h_A - h_B$}; % Middle red line
    
            \draw[color=MyRed,thick] (5.5,-0.5) -- (5.5,0.75); % Right red line
    
            % Circles
            \draw[fill=MyOrange,draw=none] (4.5,2) circle (0.2cm) node[anchor=center, shift={(0mm,0mm)}] {A} node[anchor=west, shift={(2mm,0mm)}, text=MyLightBlue] {$v_A = 0\si{\metre/\second}$};
            \draw[fill=MyOrange,draw=none] (5.5,0.75) circle (0.2cm) node[anchor=center, shift={(0mm,0mm)}] {B} node[anchor=west, shift={(2mm,0mm)}, text=MyLightBlue] {$v_B \neq 0\si{\metre/\second}$};
            \draw[fill=MyOrange,draw=none] (6.5,-0.3) circle (0.2cm) node[anchor=center, shift={(0mm,0mm)}] {C} node[anchor=west, shift={(2mm,0mm)}, text=MyLightBlue] {$v_C = v_{Max}$};
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Energieerhaltung eines Körpers in drei verschiedenen Positionen.}
    \label{Energieerhaltung eines Körpers}
\end{figure}

\paragraph{\underline{Herleitung der Eigenschaft}}

\begin{alignat*}{2}
    &E_{\text{Total Punkt A}} = E_{\text{Pot Punkt A}} = mgh_A && \\
    &E_{\text{Total Punkt B}} = E_{\text{Pot Punkt B}} + E_{\text{Kin Punkt B}} && = mgh_B + \frac{1}{2}mv_B^2 \\
                                                                              & &&= mgh_B + \frac{1}{2}m(-2sg) \\
                                                                              & &&= mgh_B + m(-sg) \\
                                                                              & &&= mgh_B + m(g(h_A - h_B)) \\
                                                                              & &&= mgh_B + mgh_A - mgh_B \\
                                                                              & &&= \underline{\underline{mgh_A}}
\end{alignat*}
\newpage

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Segelflugzeug hebt von der Startbahn mit $360\si{\kilo\metre/\hour}$ ab und gleitet am Ende des Steigfluges mit $108\si{\kilo\metre/\hour}$ horizontal entlang.\\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Wie hoch kann das Segelflugzeug steigen? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}
\begin{itemize}
    \item $\text{Punkt A: } 100\si{\metre/\second} \text{ (am Boden)} \rightarrow E_{Tot} = E_{Kin}$
    \item $\text{Punkt A: } 30\si{\metre/\second} \text{ (in der Luft)} \rightarrow E_{Tot} = E_{Kin} + E_{Pot}$
\end{itemize}
\begin{align*}
    \frac{1}{2}mv_A^2 &= mgh_B + \frac{1}{2}mv_B^2 \\
    \Rightarrow \frac{1}{2}v_A^2 &= gh_B + \frac{1}{2}v_B^2 \\
    \Rightarrow v_A^2 &= 2gh_B + v_B^2 \\
    \Rightarrow h_B &= \frac{v_A^2 - v_B^2}{2g} = \underline{\underline{463.81\si{\metre}}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Eine Gewitterwolke befand sich in $1.2\si{\kilo\metre}$ Höhe. Auf einer Fläche von $36\si{\kilo\metre}^2$ fielen $60\si{\milli\metre}$ Regen pro $\si{\metre}^2$.\\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:}
\begin{enumerate}
    \item Wieviel Kilogramm Wasser fielen auf $1\si{\metre}^2$, wieviel auf die gesamte Fläche?
    \item Welche potenzielle Energie besass das Wasser in der Wolke?
    \item Wie lange müsste ein Kraftwerk mit $P = 1'000\si{\mega\watt}$ betrieben werden, um dieselbe Energie zu erhalten wie die Wassertropfen?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item $V = 0.06\si{\metre}^3$
          \begin{itemize}
              \item \text{Auf $1\si{\metre}^2$: } $1'000\si{\kilo\gram} \cdot 0.06\si{\metre}^3 = \underline{\underline{60\si{\kilo\gram}}}$
              \item \text{Gesamtfläche: } $(1'000\si{\metre})^2 \cdot 36\si{\metre} \cdot 60\si{\kilo\gram} = \underline{\underline{2.16 \cdot 10^9\si{\kilo\gram}}}$
          \end{itemize}
    \item $W = mgh \ \rightarrow \ 2.16 \cdot 10^9\si{\kilo\gram} \cdot 9.81\si{\metre/\second}^2 \cdot 1'200\si{\metre} = \underline{\underline{2.54 \cdot 10^{13}\si{\joule}}}$
    \item $\frac{2.54 \cdot 10^{13}\si{\joule}}{1 \cdot 10^9\si{\watt}} = \underline{\underline{25'427.52\si{\second}}}$
\end{enumerate}