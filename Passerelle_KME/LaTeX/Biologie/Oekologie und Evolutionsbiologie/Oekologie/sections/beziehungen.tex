\section{Beziehungen zwischen den Lebewesen}
Da jedes Lebewesen ein Mitglied der Biozönose ist, steht es mit anderen Lebewesen in Beziehungen. Diese können für ein Individuum positive oder negative Auswirkungen haben. Man unterschiedet zwischen \emph{innerartlichen} und \emph{zwischenartlichen} Beziehungen.

\subsection{Innerartliche Beziehungen}
\subsubsection*{Kooperation und Konkurrenz}
Die meisten Lebewesen unterhalten Beziehungen zu ihren Artgenossen um sich \emph{fortpflanzen} zu können. Auch können sie bei der Nahrungssuche, Verteidigung oder Aufzucht der Nachkommen kooperieren.\\
Da aber Artgenossen die gleiche Lebensweisen und Bedürfnisse aufweisen, \emph{konkurrenzieren} sie untereinander am stärksten.\\
Tiere die eine \emph{Metamorphose} durchleben \emph{senken} ihre \emph{innerartliche Konkurrenz}, da die Jungtiere eine andere Lebensweise und Ansprüche haben als Erwachsene. Es gibt auch Arten, bei denen sich die Geschlechter anders ernähren, um die innerartliche Konkurrenz noch weiter zu senken.

\subsubsection*{Organisation des Zusammenlebens}
Um eine andauernde Konkurrenz zwischen Artgenossen zu vermeiden, besitzen Tiere angeborene Verhalten, mit denen sie ihr Zusammenleben regeln.

\paragraph*{Reviere} sind ein Gebiet mit \emph{markierten Grenzen}, das vor Artgenossen \emph{verteidigt} wird. Je nach Art wird ein Revier von einzelnen Individuen, Paaren oder Familien besetzt. Die Grenzmarkierungen können Duftmarken, Laute oder die Präsenz des Revierinhabers sein. Werden die Grenzen eines Reviers verletzt, kommt es zu \emph{ritualisierten Revierkämpfen}, welche nicht die Verletzung oder den Tod der Kontrahenten zum Ziel haben.\\
Individuen ohne Reviere \emph{pflanzen sich nicht fort}, was dazu führt, dass sich die Vermehrung an die \emph{verfügbaren Ressourcen anpasst}.

\paragraph*{Rangordnungen} regeln das Zusammenleben innerhalb von Gruppen. Dadurch werden Vorgänge wie Paarungsrecht und Reihenfolge beim Fressen geregelt. Die Rangordnung wird in den meisten Arten durch \emph{ritualisierte Kämpfe} geregelt. In anderen Arten spielen Alter und Erfahrung eine entscheidende Rolle.

\subsubsection*{Beziehung zu den Nachkommen}
Viele Lebewesen kümmern sich nach der Ablage ihrer Eier nicht mehr um ihre Nachkommen. Um die geringe Überlebenschance zu kompensieren legen sie viele Eier ab. Andere Lebewesen bilden weniger Eier/Nachkommen und erhöhen deren Überlebenschancen durch Brutfürsorge oder Brutpflege.

\paragraph*{Brutfürsorge} bezeichnet \emph{Vorsorgemassnahmen}, die zum \emph{Schutz} oder \emph{Ernährung} der Nachkommen dienen. Oft werden Nachkommen in einer geschützten Umgebung mit ausreichend Nahrung abgelegt. 

\paragraph*{Brutpflege} bezeichnet die \emph{Pflege} der Eier bzw. der Nachkommen. Beispiele für die Pflege sind: Bewachung, Versorgung, Putzen, Anlernen, etc.

\subsection{Zwischenartliche Konkurrenz und Einmischung}
Lebewesen verschiedener Arten können sich gegenseitig in die Quere kommen. So können sich Raubtiere ihre Beute streitig machen. Haben zwei Arten die selben Ansprüche, setzt sich die erfolgreichere durch und die \emph{zwischenartliche Konkurrenz} führt zum \emph{Verschwinden} der anderen \emph{Art}.

\subsubsection*{Ökologische Nische}
Die \emph{zwischenartliche Konkurrenz} hat im Verlauf der Zeit zur \emph{Spezialisierung} der Arten geführt. Durch die Spezialisierung hat jede Art ihre \emph{arteigene Umwelt} und damit ihre \emph{artspezifische ökologische Nische}. Diesen Vorgang der Spezialisierung auf ökologische Nischen nennt sich \emph{Einnischung}. Unter der ökologischen Nische versteht man \emph{alle biotischen} und \emph{abiotischen Umweltfaktoren}, die für eine Art von \emph{Bedeutung} sind. Da aber eine Nische viele Faktoren umfasst, beschränkt man sich oft auf den Ökofaktor, der für die aktuelle Beobachtung relevant ist.\\
Der Raum, in dem eine Art lebt wird als \emph{Habitat} oder \emph{Standort} bezeichnet. Jedes Ökosystem hat sogenannte \emph{Planstellen}, die durch Besetzung zu ökologischen Nischen werden.

\paragraph*{Darwinfinken} sind ein Musterbeispiel für die Einnischung. Sie sind alle nah miteinander verwandt, kommen sich aber durch ihre starke Spezialisierung nicht in die Quere.

\subsubsection*{Konkurrenzausschluss}
Wie sich zwei Populationen in ihrer Entwicklung beeinflussen lässt sich experimentell im Labor ermitteln. Durch seine Versuche formulierte G.F. Gause das Gause-Prinzip, auch \emph{Konkurrenz\-aus\-schluss-Prinzip} genannt.

\paragraph*{Konkurrenzausschluss-Prinzip} sagt aus:
\begin{center}
    \emph{In einer Biozönose leben nie zwei Arten mit gleicher ökologischen Nische.}
\end{center} 
Überlappen die Nischen zweier Arten zu stark, setzt sich die erfolgreichere Art durch und die andere Population wird verdrängt.

\subsubsection*{Ursachen der Verdrängung}
Die Verdrängung geschieht in den meisten Fällen durch den \emph{höheren Fortpflanzungserfolg} einer Art. Nur in seltenen Fällen schädigt eine Art die andere direkt (\emph{Interferenz}).\\
Ein Beispiel für Interferenz sind Pilze die in direkter Konkurrenz mit Bakterien stehen. Der Pilz \textit{Penicillium} produziert das Antibiotikum Penicillin, welches Bakterien tötet.

\subsubsection*{Optimum und Verbreitung}
Die Konkurrenzstärke beeinflusst die Verbreitung der Arten. \emph{Konkurrenzstarke} Arten leben dort, wo die \emph{Bedingungen} für sie \emph{besten} sind. \emph{Konkurrenzschwache} Arten werden an den \emph{Rand ihres Existenzeberichs} gedrängt.\\
Im Optimum wird das \emph{physiologische Optimum} bei \emph{isolierten Populationen} und das \emph{ökologische Optimum} in der \emph{natürlichen Biozönose} unterschieden.

\subsubsection*{Äquivalente Planstellen}
In Ökosystemen mit \emph{ähnlichen Bedingungen} gibt es \emph{äquivalente Planstellen}. Diese sind aber nicht überall durch die selben Arten besetzt. Diese unterschiedliche Besetzung ist durch \emph{geografische Isolation} entstanden und kann durch invasive Arten gefährdet werden.

\subsection{Fressfeind-Beute-Beziehung}
\subsubsection*{Arten von Fressfeinden}
Fressfeinde sind Konsumenten, die sich von anderen Lebewesen ernähren. Man unterscheidet \emph{Räuber}, \emph{Pflanzenfresser} und \emph{Allesfresser}. Räuber fressen andere Tiere mehr oder weniger vollständig. Pflanzenfresser fressen meistens nur Teile einer Pflanzen, während Allesfresser Tiere und Pflanzen verspeisen. Während sich Pflanzen nur passiv verteidigen können, versuchen Beutetiere aktiv ihren Räubern zu entkommen.

\subsubsection*{Nahrungserwerb}
Die Methoden des Nahrungserwerb ist je nach Nahrung verschieden:
\begin{itemize}
    \item \emph{Jäger} erjagen ihre Beute.
    \item \emph{Fallensteller} fangen ihre Beute mit Fallen.
    \item \emph{Filtrierer} filtern ihre Beute aus dem Wasser.
    \item \emph{Strudler} erzeugen einen Wasserstrom durch ihren Körper und filtern diesen.
    \item \emph{Bodenbewohner} fressen sich durch den Boden und verdauen die darin enthaltenen organischen Reste.
    \item \emph{Sammler} picken Fressbares auf.
    \item \emph{Weidegänger} beissen oder reissen Pflanzenteile ab.
\end{itemize}

\subsubsection*{Feindabwehr}
Beutetiere versuchen ein Zusammentreffen mit einem Raubtier zu vermeiden oder fliehen vor diesem.\\
Beutetiere sind so \emph{getarnt}, dass Raubtiere sie nicht sehen können. Einige Insekten \emph{schmecken} ihren Fressfeinden \emph{nicht} oder \emph{stechen} zu wenn sie sich bedroht fühlen. \emph{Gift} ist ein häufiges Abwehrmittel der Amphibien.\\
Verschiedene Tiere tragen eine \emph{Warntracht}, um Raubtiere vor ihrer Giftigkeit zu warnen. Unter Mimikry versteht sich die Nachahmung einer Warntracht, ohne dass das Tier selber giftig ist.\\
Eine weitere Art sich vor Feinden zu schützen ist die Bildung eines stabilen Gehäuses.\\
\emph{Schwarmbildung} konzentriert die Individuen auf einen Punkt, wodurch der Leerraum in dem ein Jäger nichts findet grösser wird. Auch erhöht sie die Überlebenschancen der einzelnen Exemplare, da sich ein Räuber nur schwer auf ein Tier konzentrieren kann.

\subsubsection*{Frass-Schutz bei Pflanzen}
Pflanzen schützen sich mittels \emph{Giften}, \emph{Abwehrdüften}, \emph{Stacheln}, \emph{Dornen}, \emph{Brennhaaren} etc. vor ihren Fressfeinden.\\
Einige Pflanzen rekrutieren andere Tierarten, um sich vor ihren Fressfeinden zu schützen.\\

\newpage

\subsection{Parasit-Wirt-Beziehungen}
\subsubsection*{Schädliche Nutzniesser}
Parasiten \emph{leben auf} oder \emph{in} ihrem \emph{Wirt} und \emph{ernähren} sich von ihm. Sie schaden ihrem Wirt aber töten ihn nicht. Parasiten sind kleiner und kurzlebiger als ihr Wirt. Parasit-Wirts-Beziehungen sind das Resultat einer engen \emph{Koevolution}.\\
Parasiten können als \emph{permanente} Parasiten ständig im Wirt leben oder als \emph{temporäre} Parasiten den Wirt nur gelegentlich besuchen.\\
Nach ihrem Aufenthaltsort unterscheidet man \emph{Ektoparasiten auf} und \emph{Endoparasiten im} Wirt.\\
Schaden fügen Parasiten ihren Wirten nicht direkt durch ihren Konsum, sondern durch ihre \emph{Ausscheidungen} zu.\\
Permanente Parasiten sind meistens \emph{stenök} und \emph{wirtsspezifisch}. Um jedoch ihren Wirt nicht zu überlasten, müssen die Nachkommen eines Parasiten den Wirtsorganismus wechseln. Diese satteln auf einen \emph{Zwischenwirt} um, welcher dann als Überträger fungiert. Erfolgreiche Parasiten zeichnen sich durch ihre Langlebigkeit und die Fähigkeit, ihren Wirt so zu beeinflussen, dass ihre eigenen Überlebenschancen steigen, aus.

\subsubsection*{Viren, Bakterien und Pilze}
Biologisch gesehen gelten \emph{Krankheitserreger} als Parasiten, werden aber in der Medizin von Parasiten im engeren Sinne unterschieden.\\
\emph{Viren} haben keinen eigenen Stoffwechsel und können sich auch nicht selber fortpflanzen. Sie \emph{befallen Wirtszellen} und \emph{programmieren} diese um.\\
\emph{Parasitäre Bakterien} leben auf oder im Wirt und schaden ihm mit ihren Ausscheidungen.\\
\emph{Parasitäre Pilze} leben meist nahe an der Oberfläche eines Wirts, da sie sich durch Sporen verbreiten.

\subsubsection*{Tiere als Ektoparasiten}
Ektoparasiten zeichnen sich durch folgende Merkmale aus:
\begin{itemize}
    \item \emph{Stech-} und \emph{Saugapparat} zur Blutentnahme.
    \item \emph{Haft-} und \emph{Klammervorrichtungen}.
    \item Fehlende Augen, dafür gut ausgeprägter und spezialisierter \emph{Wärme-} und \emph{Geruchssinn}.
    \item Können \emph{lange} Zeiten \emph{ohne Nahrung} auskommen.
    \item Injizierbarer \emph{Speichel} der \emph{Blutgerinnung verhindert}.
\end{itemize}
Ektoparasiten richten durch die Blutentnahme keinen nennenswerten Schaden an, können aber gefährliche \emph{Krankheitserreger übertragen}.

\newpage

\subsubsection*{Tiere als Endoparasiten}
\paragraph*{Darmparasiten} nutzen den Darm als Lebensraum.
\begin{center}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{7cm} | p{7cm}}
        \textbf{Vorteile} & \textbf{Nachteile}\\
        Grosses und konstantes Angebot an verdauter \emph{Nahrung}. & \emph{Verdauungsenzyme} verdauen Eindringlinge.\\
        \emph{Konstante Temperatur}. & \emph{Kein Sauerstoff}.\\
        \emph{Keine Feinde}. & Peristaltik drückt den Darminhalt Richtung Ausgang.\\
        & \emph{Erschwerte Fortpflanzung}, Nachkommen müssen einen neuen Wirtsorganismus finden.
    \end{tabular}
\end{center}

Darmparasiten wie Bandwürmer sind jedoch an diese Umgebung angepasst:
\begin{itemize}
    \item \emph{Kein eigenes Verdauungssystem}, die bereits verdaute Nahrung wird über die Haut aufgenommen.
    \item \emph{Haken} und \emph{Saugnäpfe} zur Fixierung an der Darmwand.
    \item \emph{Schutzschicht} auf der Haut (\emph{Caticula}) gegen die Verdauungsenzyme des Wirts.
    \item Energiegewinnung durch \emph{Gärung} um das Fehlen von Sauerstoff zu umgehen.
    \item Bandwürmer sind \emph{selbstbefruchtende Zwitter}.
    \item \emph{Riesige Eierzahl} als Ausgleich gegenüber den geringen Überlebenschancen.
    \item Bandwürmer bestehen aus vielen Segmenten, von denen das Hinterste mit Eiern gefüllt und abgeworfen wird. Über einen Zwischenwirt kommt es zu Wirtswechsel.
\end{itemize}

\paragraph*{Blutparasiten}
Kleine Endoparasiten (Einzeller oder Fadenwürmer) können im Blut- oder Lymphsystem eines Wirts leben. Übertragen werden sie durch blutsaugende Insekten.\\
Blut bietet als Lebensraum folgende Vor- und Nachteile:
\begin{center}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{7cm} | p{7cm}}
        \textbf{Vorteile} & \textbf{Nachteile}\\
        Grosses und konstantes Angebot an \emph{Nährstoffen} und \emph{Sauerstoff}. & Wirt verteidigt sich mit seinen \emph{Abwehrmechanismen} (Leukozyten, Antikörper).\\
        Konstante abiotische Faktoren. & Die \emph{Übertragung} der \emph{Nachkommen} in einen neuen Wirt gestaltet sich schwierig.
    \end{tabular}
\end{center}

\subsubsection*{Tiere als Parasiten von Pflanzen}
Tiere die ihre \emph{Eier in Pflanzenteile ablegen}, gelten auch als Parasiten. Als Reaktion auf den Befall bildet die Pflanze eine Galle, die das Insekt oder Ei umschliesst und ihm als Nahrung dient. Die Pflanzengalle ist eine Anomalie im Wachstum der Pflanze, die durch fremde Organismen verursacht wird.

\subsubsection*{Höhere Pflanzen als Parasiten}
Unter den höheren Pflanzen gibt es nur wenige Parasiten. Man unterscheidet zwischen Halb- und Vollparasiten. \emph{Halbparasiten} sind \emph{autotroph}; d.h. sie beziehen vom Wirt nur Wasser und Mineralstoffe. \emph{Vollparasiten} sind \emph{heterotroph}; d.h. sie entziehen dem Wirtsorganismus neben Wasser auch organische Stoffe. Dies erreichen sie, indem sie die Assimilatleitungen anzapfen.

\subsection{Symbiosen}
\emph{Symbiose} bezeichnet ein \emph{Zusammenleben}, das \emph{beiden} Beteiligten etwas \emph{nützt}. Unterscheiden sich die beiden Partner in ihrer Grösse, nennt man den kleineren \emph{Symbionten} und den grösseren \emph{Wirt}.\\
Viele Symbiosen entstehen im Zusammenhang mit dem \emph{Nahrungserwerb}, der \emph{Fortpflanzung} oder dem \emph{Schutz vor Feinden}. Die beiden Partner \emph{kontrollieren} sich gegenseitig und stellen sicher, dass der andere seiner Pflicht nachkommt.

\subsubsection*{Flechten}
Flechten sind \emph{Pionierpflanzen} und leben auch in den härtesten Regionen. Sie bilden eine Symbiose aus \emph{Pilzen} und \emph{Algen}, die zusammen einen \emph{Superorganismus} bilden. Die autotrophen Algen \emph{synthetisieren Zucker} und geben einen Teil davon an die Pilze ab. Der Pilz \emph{schützt} und \emph{versorgt} die Algen mit Wasser und Mineralstoffen. 

\subsubsection*{Mykorrhiza}
Mykorrhiza ist eine Symbiose aus \emph{Pilzen} und den \emph{Wurzeln} von Bäumen. Der Pilz erhält Zucker und liefert dafür Wasser und Mineralstoffe.

\subsubsection*{Blütenpflanzen und Bestäuber}
Fast alle \emph{Blütenpflanzen} stehen mit Tieren, die sie \emph{bestäuben}, in Symbiose. Meistens sind es Insekten, die \emph{Pollen} mit den männlichen Gameten übertragen. Da Pflanzen nur durch Pollen der selben Art bestäubt werden können, haben sie sich auf Bestäuber \emph{spezialisiert}, die nur selten oder nie andere Pflanzenarten anfliegen.\\
\glqq Hauptberufliche\grqq{} Bestäuber (Bienen) besitzen einen Magen, indem sie den Nektar transportieren und Sammelbeine, an denen grosse Mengen an Pollen transportiert werden können.

\subsubsection*{Blütenpflanzen und Verbreiter}
Viele Pflanzen sind bei der \emph{Verbreitung} ihrer \emph{Samen} auf Tiere angewiesen. Meist dient das Fruchtfleisch als Belohnung für das Tier, das die Frucht frisst und die \emph{Samen} zu einem späteren Zeitpunkt wieder \emph{ausscheidet}.

\subsubsection*{Knöllchenbakterien und Hülsenfrüchtler}
Hülsenfrüchtler wie Bohnen, Erbsen oder Klee besitzen kleine kugelförmige \emph{Wucherungen} des Wurzelgewebes, die \emph{Knöllchenbakterien} enzhalten. Diese Bakterien können den elementaren \emph{Stickstoff binden}.

\subsubsection*{Weiter Symbiosen}
\emph{Blattschneiderameisen} kultivieren \emph{Pilze} und ernähren sich von diesen. Auch gehen Ameisen Symbiosen mit \emph{Blattläusen} ein, da diese überschüssigen Zucker sekretieren. Die Ameisen schützen sie vor Feinden und tragen sie zu geeigneten Pflanzenteilen.\\
\emph{Korallenpolypen} gehen mit \emph{Algen} eine Symbiose ein. Die Polypen profitieren von den organischen Produkten der Algen und schützten und versorgen sie im Gegenzug dafür. Manche Meeresschnecken nehmen diese Algen beim Verzehr von Korallen auf, woraufhin die Algen eine Symbiose mit den Schnecken eingehen.\\
Manche \emph{Einsiedlerkrebse} gehen mit \emph{Seeanemonen} Symbiosen ein. Die Seeanemone profitiert von den Nahrungsresten des Krebs und unterstützt den Krebs bei der Verteidigung.\\
\emph{Putzerfische} oder \emph{Putzergarnelen} ernähren sich von Parasiten, die auf der Haut oder im Kiemenraum von Fischen leben.\\
Einige Meeresbewohner beherbergen und versorgen lichtproduzierende Bakterien, die als Gegenleistung Licht erzeugen.