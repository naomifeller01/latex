\section{Mechanismen der Evolution}
\subsection{Veränderung des Genoms}
Veränderungen des Genoms führen zu veränderten Lebewesen und sind deswegen essentiell für die Evolution.

\subsubsection*{Mutation}
Unter \textbf{Mutation} versteht man die Veränderung der genetischen Information. Mutationen können \textbf{spontan} auftreten oder durch \textbf{mutagenen Stoffe}\footnote{Mutagene Stoffe sind unter anderem radioaktive Strahlung, UV-Licht und gewisse Chemikalien.} hervorgerufen werden.\\
Mutationsarten:
\begin{itemize}
    \item Punktmutation
    \item Chromosomen- oder Strukturmutationen
    \item Genommutationen
\end{itemize}

\paragraph*{Punktmutationen}
Bei Punktmutationen werden \textbf{einzelne Basen} der DNA ausgetauscht. Hat eine Punktmutationen keine Auswirkung, weil zum Beispiel das Triplett mit ausgetauschten Base immer noch für die gleiche Aminosäure codiert, handelt es sich um eine stumme Mutation.\\
Punktmutationen können folgende Konsequenzen haben:
\begin{itemize}
    \item Veränderung der Aminosäuren-Sequenz.
    \item Abbruch der Proteinkette, weil ein normales Codon zu einem Stoppcodon mutiert.
    \item Kettenverlängerung, wenn ein Stoppcodon zu einem normalen Codon mutiert.
\end{itemize}

Wird eine einzelne Base \textbf{hinzugefügt} oder \textbf{entfernt} kommt es zu einer \textbf{Rastermutation}.

\paragraph*{Chromosomenmutationen oder Strukturmutationen}
Chromosomenmutationen umfassen grössere Bereiche eines Chromosoms und somit eine grössere Anzahl von Genen. \\
Bei \textbf{ungleichem Crossing-over} sind die ausgetauschten Elemente \textbf{unterschiedlich lang}. Neben diesem Phänomen kann es auch zum Verlust von Chromosomenteilen oder dem Verschmelzen von Chromosomen kommen.\\
Chromosomenmutationen können zu starken Veränderungen führen und sind wahrscheinlich deswegen von grosser Bedeutung für die Evolution.

\paragraph*{Genommutationen}
Bei Genommutationen wird die Anzahl der Chromosomen verändert. Werden ganze Genome vervielfacht spricht man von Polyploidisierung.\\
Polyploidisierung kommt bei Tieren selten vor, scheint aber für Pflanzen ein wichtiger Ausgangspunkt für die Artbildung zu sein.

\subsubsection*{Auswirkungen von Mutationen auf den Phänotyp}
Mutationen in den Gameten führen zu Lebewesen mit zufälliger genetischer Variabilität und dementsprechendem verändertem Phänotyp.\\
Die neuen Phänotypen müssen müssen sich behaupten und fortpflanzen. Da nur erfolgreiche Varianten nachkommen zeugen, bleiben nur vorteilhaft Eigenschaften erhalten.

\subsubsection*{Mutationsraten}
Die \textbf{Mutationsrate} beim \textbf{Menschen} beträgt etwa $30$ Mutationen pro $1$ Milliarde Basenpaare. Das heisst die Mutationsrate beträgt ca. \textbf{$3\cdot 10^{-8}$}. Da das diploide menschliche Genom um die $6\cdot 10^9$ Basenpaare umfasst, trägt jeder Mensch etwa $200$ neue Mutationen in sich.\\
Betrachtet man eine Population von $500~000$ Menschen, sammeln sich pro Generation $100$ Millionen Mutationen an. Wenn man bedenkt, dass nur $2.5\%$ des Genoms codierend sind, kommt man auf $2.5$ Millionen Genvarianten pro Generation.

\subsubsection*{Rekombination}
Die zweite Ursache für die Zunahme der Variabilität ist die geschlechtliche Fortpflanzung. Bei der Meiose wird der diploide Chromosomensatz auf vier haploide Zellen verteilt. Dabei kommt es zur Neuanordnung des genetischen Materials (\textbf{Rekombination}). Die möglichen Konfigurationen der Rekombination beträgt beim Menschen $2^{46}$ Möglichkeiten. Durch Crossing-over zu Beginn der Meiose wird die Anzahl dieser Möglichkeiten um ein Vielfaches erhöht.

\subsubsection*{Vergleich Rekombination - Mutation}
Rekombination ist für die Vielfalt der Genotypen bedeutsamer als die Mutation. Denn Rekombination kommt zum einen weitaus häufiger vor, zum anderen Arbeitet sie mit bereits bewährten Methoden. Mutationen schaffen zwar Neues, müssen sich aber erstmals bewähren und fallen in den meisten Fällen negativ aus.

\subsection{Selektion}
Die Auslese der am besten angepassten Individuen aus allen entstandenen Varianten nennt sich \textbf{natürliche Selektion}. Der Schlüssel zur natürlichen Selektion ist der \textbf{relative Fortpflanzungserfolg} (auch \textbf{Fitness} genannt). Dabei wird die Zahl der erfolgreich gezeugten Nachkommen mit der anderer Individuen aus der gleichen Population verglichen.\\
Ein höherer Fortpflanzungserfolg kann durch folgende Faktoren erreicht werden:
\begin{itemize}
    \item \textbf{Höhere Lebenserwartung} und damit mehr Kontakt zu Partnern.
    \item Grössere Chance einen Geschlechtspartner zu finden, zu \glqq erobern\grqq{} (Brutkämpfe) und von diesem akzeptiert zu werden.
    \item \textbf{Erhöhte Fruchtbarkeit} (Fertilität) und damit beispielsweise mehr Nachkommen pro Wurf.
    \item \textbf{Geringer Generationenabstand} und dementsprechend frühere Geschlechtsreife.
\end{itemize}
Trotzdem, dass die Selektion eine sehr grosse Rolle in der Evolution spielt, darf man nicht annehmen, dass jedes Allel einen objektiven Vor- oder Nachteil bringt. \textbf{Adaptiv neutrale Allele} bergen keinen direkten Vor- oder Nachteil und bei ihnen entscheidet der Zufall, ob sie erhalten bleiben oder nicht.

\paragraph*{Birkenspanner}
Der Birkenspanner ist ein relativ heller Nachtfalter, der durch Mutation auch in einer dunklen fast schwarzen Farbe auftreten kann. Dunkle Birkenspanner werden jedoch von Räubern schnell entdeck und gefressen. Während der industriellen Revolution wurden die Ruheplätze der Birkenspanner durch Russ beinahe schwarz gefärbt, wodurch die dunklen Birkenspanner plötzlich eine höhere Fitness besassen, da sie sich nun besser verstecken konnten als ihre hellen Artgenossen.\\
Die dunkle Farbe ist das Resultat einer Mutationen und keine aktive oder passive Reaktion auf ihre Umgebung. Einzig die Selektion sorgte dafür, dass die dunklen Birkenspanner lange genug überlebten um sich fortzupflanzen.\\

\subsubsection*{Wirkungen der Selektion}
Die Selektion kann sich auf eine Population auf folgende Arten auswirken:
\begin{itemize}
    \item Stabilisierend
    \item Gerichtet (dynamisch)
    \item Aufspaltend (disruptiv)
\end{itemize}

\begin{figure}[h!]
    \centering
        \begin{tikzpicture}
            \begin{groupplot} [
                group style = {
                    group size = 3 by 2,
                    vertical sep = 2cm,
                    horizontal sep = 2cm
                }, 
                width = 4.5cm, 
                height = 4.5cm,
                ymin = 0,
                ymax = 10,
                xmin = 0,
                xmax = 20,
                yticklabels={,,},
                ytick style = {draw = none},
                xticklabels={,,},
                xtick style = {draw = none},
                inner axis line style={thick},
                axis lines=center,
                axis line style={thick},
                ylabel = {{\scriptsize Anzahl Individuen}},
                xlabel = {{\scriptsize Merkmalsausprägung}},
                x label style={at={(axis description cs:0.5,0)},anchor=north},
                y label style={at={(axis description cs:0,.5)},rotate=90,anchor=south},
                clip=false
                ]
                \nextgroupplot
                \node[above,font=\bfseries] at (current bounding box.north) {Stabilisierende Selektion};
                \draw[dashed] (axis cs: 10, 0) -- (axis cs: 10, 7.9);
                \draw[thick, rounded corners, darkred] (axis cs: 3, 0) to[out = 55, in = 210] (axis cs: 10, 8) to[out = 330, in = 125] (axis cs: 17, 0);
                \draw[->, > = stealth] (axis cs: 0.5, 1) -- (axis cs: 3.5, 1);
                \draw[->, > = stealth] (axis cs: 10, 8.2) -- (axis cs: 10, 10);
                \draw[->, > = stealth] (axis cs: 20, 1) -- (axis cs: 16.5, 1);

                \path (axis cs: 0.5, 1) -- (axis cs: 3.5, 1) node [above, midway] {{\scriptsize $-$}};
                \path (axis cs: 10, 10) -- (axis cs: 10, 8.2) node [right, midway] {{\scriptsize $+$}};
                \path (axis cs: 20, 1) -- (axis cs: 16.5, 1) node [above, midway] {{\scriptsize $-$}};

                \coordinate (stab1) at (axis cs: 10, -1.5);

                \nextgroupplot
                \node[above,font=\bfseries] at (current bounding box.north) {Gerichtete Selektion};
                \draw[dashed] (axis cs: 10, 0) -- (axis cs: 10, 7.9);
                \draw[thick, rounded corners, darkred] (axis cs: 3, 0) to[out = 55, in = 210] (axis cs: 10, 8) to[out = 330, in = 125] (axis cs: 17, 0);
                \draw[->, > = stealth] (axis cs: 0.5, 1) -- (axis cs: 3.5, 1);
                \draw[->, > = stealth] (axis cs: 10, 8.2) -- (axis cs: 10, 10);
                \draw[->, > = stealth] (axis cs: 16.5, 1) -- (axis cs: 20, 1);

                \path (axis cs: 0.5, 1) -- (axis cs: 3.5, 1) node [above, midway] {{\scriptsize $-$}};
                \path (axis cs: 10, 10) -- (axis cs: 10, 8.2) node [right, midway] {{\scriptsize $+$}};
                \path (axis cs: 20, 1) -- (axis cs: 16.5, 1) node [above, midway] {{\scriptsize $+$}};

                \coordinate (ger1) at (axis cs: 10, -1.5);

                \nextgroupplot
                \node[above,font=\bfseries] at (current bounding box.north) {Aufspaltende Selektion};
                \draw[dashed] (axis cs: 10, 0) -- (axis cs: 10, 7.9);
                \draw[thick, rounded corners, darkred] (axis cs: 3, 0) to[out = 55, in = 210] (axis cs: 10, 8) to[out = 330, in = 125] (axis cs: 17, 0);
                \draw[->, > = stealth] (axis cs: 3.5, 1) -- (axis cs: 0.5, 1);
                \draw[->, > = stealth] (axis cs: 10, 10) -- (axis cs: 10, 8.2);
                \draw[->, > = stealth] (axis cs: 16.5, 1) -- (axis cs: 20, 1);

                \path (axis cs: 0.5, 1) -- (axis cs: 3.5, 1) node [above, midway] {{\scriptsize $+$}};
                \path (axis cs: 10, 10) -- (axis cs: 10, 8.2) node [right, midway] {{\scriptsize $-$}};
                \path (axis cs: 20, 1) -- (axis cs: 16.5, 1) node [above, midway] {{\scriptsize $+$}};

                \coordinate (auf1) at (axis cs: 10, -1.5);

                \nextgroupplot
                \draw[dashed] (axis cs: 10, 0) -- (axis cs: 10, 9.9);
                \draw[thick, rounded corners, darkred] (axis cs: 5, 0) to[out = 70, in = 230] (axis cs: 10, 10) to[out = 310, in = 110] (axis cs: 15, 0);

                \coordinate (stab2) at (axis cs: 10, 10.5);

                \nextgroupplot
                \draw[dashed] (axis cs: 12, 0) -- (axis cs: 12, 7.9);
                \draw[thick, rounded corners, darkred] (axis cs: 5, 0) to[out = 55, in = 210] (axis cs: 12, 8) to[out = 330, in = 125] (axis cs: 19, 0);

                \coordinate (ger2) at (axis cs: 12, 8.5);

                \nextgroupplot
                \draw[dashed] (axis cs: 6, 0) -- (axis cs: 6, 7.9);
                \draw[dashed] (axis cs: 12, 0) -- (axis cs: 12, 7.9);
                \draw[thick, rounded corners, darkred] (axis cs: 0, 0) to[out = 70, in = 220] (axis cs: 6, 8) to[out = 310, in = 110] (axis cs: 9, 4.5) to[out = 70, in = 230] (axis cs: 12, 8) to[out = 320, in = 110] (axis cs: 18, 0);

                \coordinate (auf2) at (axis cs: 6, 8.5);
                \coordinate (auf3) at (axis cs: 12, 8.5);
            \end{groupplot}

            \draw[->, > = stealth, thick] (stab1) -- (stab2);
            \draw[->, > = stealth, thick] (ger1) -- (ger2);
            \draw[->, > = stealth, thick] (auf1) -- (auf2);
            \draw[->, > = stealth, thick] (auf1) -- (auf3);
        \end{tikzpicture}
    \caption{Stabilisierende, gerichtete und aufspaltende Selektion}
    \label{fig:Selektion}
\end{figure}

\paragraph*{Stabilisierende Selektion}
Eine \textbf{stabilisierende Selektion beseitigt extreme Varianten} und hält die Variabilität stabil. Sie tritt auf, wenn eine Population optimal an eine stabile Umgebung angepasst ist und sich Abweichungen eher Nachteilig auswirken.
In gewissen Vogelarten verunfallen Individuen mit kürzeren oder längeren Flügelspannen häufiger als solche mit normaler Spannweite. Als Folge werden erstere \glqq aussortiert\grqq{}

\paragraph*{Gerichtete oder dynamische Selektion}
Eine \textbf{gerichtete oder dynamische Selektion} tritt bei veränderten Umweltbedingungen auf und \textbf{verändert den Genpool in eine bestimmte Richtung}. Sie erweitert auch in gewissen Fällen den Lebensraum einer Spezies.\\
Das Hämoglobin von Kamelen und Lamas (Gruppe der Paarhufer) unterscheidet sich durch eine einzige Aminosäure. Diese Aminosäure führt aber zu einer extremen Sauerstoffaffinität und ermöglicht ihnen in grösseren Höhen noch zu grasen.

\paragraph*{Aufspaltende oder disruptive Selektion}
Die \textbf{aufspaltende oder disruptive Selektion} reagiert auf regionale Unterschiede und führt zu \textbf{unterschiedlichen Unterarten}.\\
Kupfer- oder bleihaltige Bergwerkhalden sind für die meisten pflanzen toxisch, können aber von bestimmten resistenten Gräsern besiedelt werden. Diese können sich aber gegen normale Gräser nicht behaupten, da sie langsamer wachsen und fruchten. 

\subsubsection*{Selektionsfaktoren}
Ein Selektionsfaktor ist ein \textbf{Umweltfaktor}, der einen \textbf{Einfluss auf die Fitness} eines Individuums hat. Er erzeugt einen Selektionsdruck indem er die Phänotypen mit der höchsten Fitness unter den gegebenen Umständen \glqq Auswählt\grqq{}.\\
Selektionsfaktoren sind:
\begin{itemize}
    \item abiotische Selektionsfaktoren
    \item biotische Selektionsfaktoren
    \item künstliche Selektionsfaktoren
\end{itemize}

\paragraph*{Abiotische Faktoren}
Abiotische Selektionsfaktoren sind unter anderem Wärme und Kälte, Feuchte und Trockenheit, Licht und Dunkelheit sowie Luft- und Wasserströmungen.\\
Ein Fallbeispiel für abiotische Selektion sind die Insekten auf den Kerguelen (Inselgruppe im Süden des indischen Ozeans). Auf den Inseln kommt es zu einer Häufung von flugunfähigen Insekten. Diese Adaption lässt sich dadurch erklären, dass auf den Kerguelen ein starker Wind bläst. Flugunfähige Insekten können nicht auf den Ozean geblasen werden und haben in diesem Fall keine Fressfeinde zu fürchten.

\paragraph*{Biotische Faktoren}
\textbf{Biotische Selektionsfaktoren} gehen von der \textbf{belebten Umwelt} aus. Dabei kann es sich um Einflüsse der eigenen oder anderer Arten handeln. Ein Beispiel für für biotische Selektionsfaktoren ist die Dynamik zwischen Räuber und Beutetieren.\\
Die Sichelzellanämie führt dazu, dass Erythrozyten unter gewissen Bedingungen eine Sichelform annehmen, was zu schweren Durchblutungsstörungen führt, die früher oder später tödlich enden. Jedoch ist diese Krankheit in den Tropen weit verbreitet. Die Erklärung liegt darin, dass heterozygote Individuen nicht an Sichelzellanämie erkranken und gleichzeitig gegen Malaria resistent sind. Da sie nicht an Malaria erkranken können leben sie länger und können mehr Nachkommen zeugen. Der Malariaerreger wird so zum biotischen Selektionsfaktor.

\paragraph*{Sexuelle Selektion}
Bei der \textbf{sexuellen Selektion} geht es um die \textbf{Konkurrenz um Paarungspartner} und somit um die Selektion innerhalb einer Art. Sie ist eine Sonderform der biotischen Selektion.\\
Unter \textbf{intersexuellen Selektion} versteht man die \textbf{aktive Auswahl} des Fortpflanzungspartners. Dies Geschieht in den meisten Fällen durch die Weibchen. Sie trachten nach einem guten Geschlechtspartner, da ihre Investition in die Fortpflanzung um einiges höher ist als die der Männchen.\\
Bei der \textbf{intrasexuellen Selektion} geht es um das \textbf{Ausmachen des Paarungsrechts}. In der Regel kämpfen Männchen untereinander um zu entscheiden wer sich mit den Weibchen paaren darf.\\
Wenn Weibchen ihren Fortpflanzungspartner wählen können, lassen sie oft eine Kopulation mit mehreren Männchen zu. Dies führt zur \textbf{Spermienkonkurrenz}, bei der Männchen noch nach der Paarung untereinander konkurrieren müssen. Dieser Selektionsdruck führt zu einer Vergrösserung der Hoden.

\paragraph*{Koevolution}
\textbf{Koevolution} kommt dann zustande, wenn sich \textbf{zwei Arten} bei der Selektion \textbf{gegenseitig beeinflussen}. Das klassische Beispiel für Koevolution sind die Bestäubungsmechanismen zwischen Tieren und Pflanzen.\\
Bei einigen Blütenpflanzen ist die Konstruktion der Blüte an bestimmte Insekten angepasst, die dadurch diese Pflanze häufiger ansteuern und dadurch zum Hauptbestäuber werden.

\paragraph*{Künstliche Selektionsfaktoren}
\textbf{Künstliche Selektionsfaktoren} gehen vom \textbf{Menschen} aus. Dabei werden Tier- und Pflanzenarten an die Bedürfnisse des Menschen angepasst. Sämtliche Kulturpflanzen, sowie Nutz- und Haustiere sind das Resultat dieser künstlichen Selektion.

\subsubsection*{Konvergente Evolution}
Bei der \textbf{konvergenten Evolution} (kurz: \textbf{Konvergenz}) wirken \textbf{gleiche Selektionsfaktoren} auf \textbf{verschiedene Arten}. Dies führt zu \textbf{ähnlichen Anpassungsformen}, obwohl zwischen den Arten keine nähere Verwandtschaft besteht.\\
Die grosse Wasserknappheit führt bei Kakteen (Amerika) und Wolfsmilchgewächsen (Afrika) zu sehr ähnlichen Wachstumsformen.

\subsection{Gendrift}
\textbf{Gendrift} oder \textbf{genetischer Drift} bezeichnet die \textbf{zufällige Veränderung der Allelfrequenz} einer Population. Er ist eine weitere Ursache für den evolutionären Wandel.\\
Der Gendrift kann verschiedene Ursachen haben und kommt zum Beispiel bei folgenden Gegebenheiten vor:
\begin{itemize}
    \item Erstbesiedlung von Inseln.
    \item Auswanderung in ein neues Gebiet.
    \item Neubeginn nach Katastrophen-Ereignissen.
    \item Unterteilung des Lebensraums, die den Allel-Austausch verhindern.
\end{itemize}
Die Gemeinsamkeit dieser Umstände ist, dass einige wenige Individuen vom Rest der Population abgetrennt werden. Dadurch \textbf{vermindert} sich der \textbf{Genpool} drastisch. Die neue Population besitzt dadurch eine geringe genetische Variabilität. Man spricht deswegen auch von einem \textbf{Reduktionsereignis}, \textbf{Gründerpopulation} und \textbf{Gründereffekt}.\\
Der Gründereffekt kann zur Entstehung neuer Arten führen (Darwinfinken).\\
Ein anderes Beispiel für den Gründereffekt betrifft die Abstammung des Menschen ausserhalb Afrikas. Alle Menschen ausserhalb Afrikas stammen von der selben Gründerpopulation ab. Das führt dazu, dass die genetische Variabilität innerhalb Afrikas weitaus grösser ist als die ausserhalb.\\

Vermindert sich die Zahl einer Population innerhalb kürzester Zeit und erholt sie sich wieder, spricht man von einem \textbf{Flaschenhalseffekt}. Die genetische Variabilität lässt sich auf die wenigen überlebenden Individuen zurückführen.\\
Beispiele für den Flaschenhalseffekt sind die Steinbockpopulation im Alpenraum und die Züchtung von Rassenhunden.\\

Allgemein gilt, dass ein Gendrift die \textbf{genetische Variabilität} einer Population \textbf{verringert} und so das \textbf{Potenzial} sich an \textbf{Umweltveränderungen anzupassen vermindert}.