% Fundamentalsatz der Analysis
%
% Sources: 
% https://de.wikipedia.org/wiki/Fundamentalsatz_der_Analysis

%%% PACKAGES %%%
% Standard
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}

% Formatting
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage{geometry}
\usepackage{fancyhdr} 
\usepackage[hidelinks]{hyperref}         
\usepackage{lastpage}
\usepackage{lmodern}
\usepackage{wrapfig}

% Typography
\usepackage{microtype}
\usepackage{titlesec}
\usepackage[font=small,justification=raggedright,singlelinecheck=false,skip=5pt]{caption}
\usepackage{csquotes}

% Symbols, Mathematics
\usepackage{amsmath}
\usepackage{amssymb} % \cdots
\usepackage{cancel} % Strike-through maths terms
\usepackage{siunitx} % SI-Units
\usepackage{textcomp}

% Graphics and Plots
\usepackage{graphicx}
\graphicspath{ {./Illustrations/} {../Illustrations/} }
\usepackage{epstopdf}
\epstopdfsetup{update,suffix=-conv} % Only regenerate pdf files when eps file is newer; Changes suffix of converted images from default "-eps-converted-to" to "-conv"
\usepackage{tikz}
\usepackage{pgfplots}
%\addtolength{\jot}{1em} % Adds 1em of spacing to all align environments
%\def\arraystretch{1.25} % Stretches tables by amount specified in percent
\usepackage{float}
\usepackage{xcolor}

% Plot styles
\pgfplotsset{orangemarker/.style={color=orange,only marks,mark=*, mark options={scale=0.5}}}

%%% PREAMBLE %%%
% Formatting and Title
\geometry{bottom=30mm}
\setlength\parindent{0pt}
\title{\vspace{-3cm}Erläuterung zum Zusammenhang\\der Ableitung und der Flächeninhaltsfunktion}
\author{Stefan Gloor}

\pgfplotsset{compat=1.7}                     
\usepgfplotslibrary{fillbetween}
\numberwithin{equation}{subsubsection}  % numbering of equations

\fancypagestyle{TitlePageStyle}{ %% frontpage
    \fancyhead{}
    \fancyfoot[C]{\today}
    \fancyfoot[L]{Stefan Gloor}
    \fancyfoot[R]{Seite \thepage \protect{ }von \pageref{LastPage}}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0.5pt}
}

\usetikzlibrary{    shapes,
                    arrows,            % Pfeilform
                    arrows.meta, 
                    chains, 
                    external,          % Externalizing graphics
                    positioning,        % Node distance
                    angles
                }

\pagestyle{fancy}
\fancyhf{}
\fancyhead[C]{\nouppercase{\leftmark}}
\fancyfoot[C]{\today} %% following pages
\fancyfoot[L]{Stefan Gloor}
\fancyfoot[R]{Seite \thepage \protect{ }von \pageref{LastPage}}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\renewcommand{\sectionmark}[1]{% % Removes number next to section in header
    \markboth{#1}{}}

%%% DOCUMENT %%%
\begin{document}

\maketitle
\thispagestyle{TitlePageStyle}
%\newpage

Wir betrachten die Randfunktion $f(x)$, deren Graph eine Fläche mit der X-Achse einschliesst.
Folglich lässt sich für ein beliebiges $x_0$ eine Flächeninhaltsfunktion $A(x_0)$ bilden,
deren Funktionswert dem Flächeninhalt entspricht, welcher von der Randfunktion, der X-Achse und den Geraden $x=0$ und $x=x_0$ eingeschlossen wird.

\begin{center}
    \begin{tikzpicture}[scale=0.7]
        \begin{axis}[   samples=100,
                        inner axis line style={thick},
                        axis lines=center,
                        axis line style={-Latex},
                        xticklabels={,,},
                        yticklabels={,,},
                        xmin=0,
                        xmax=1.5,
                        ymax=2.5,
                        ymin=-0.5,
                        ]

        \addplot[domain=0:3,black, thick,name path=A] {x^2};
        \addplot[domain=0:3,draw=none, thick,name path=B] {0.005};
        \addplot[blue,fill opacity=0.3] fill between[of=A and B,soft clip={domain=0:1}];

        \draw[dotted] (axis cs: 1,-0.2) -- (axis cs: 1,2.5);
        \node at (axis cs: 1.3,2.2){$f(x)$};
        \node at (axis cs: 1,-0.4){$x_0$};
        \node at (axis cs: 0.8,0.3){$A(x_0)$};

        \end{axis}
    \end{tikzpicture}
\end{center}

Jetzt kann die Überlegung gemacht werden, wie stark sich die Fläche $A(x)$ \textit{ändert} wenn sich 
$x$ um einen bestimmten Betrag ändert. Gesucht ist also der Quotient $\frac{\Delta A}{\Delta x}=\frac{A(x+\Delta x)-A(x)}{\Delta x}$. 
Das entspricht der Ableitung, $\frac{\mathrm{d}}{\mathrm{d}x}A(x)=A'(x)$.

\begin{center}
    \begin{tikzpicture}[scale=0.9]
        \begin{axis}[   samples=100,
                        inner axis line style={thick},
                        axis lines=center,
                        axis line style={-Latex},
                        xticklabels={,,},
                        yticklabels={,,},
                        xmin=0,
                        xmax=1.5,
                        ymax=2.5,
                        ymin=-0.5,
                        ]

        \addplot[domain=0:3,black, thick,name path=A] {x^2};
        \addplot[domain=0:3,draw=none, thick,name path=B] {0.005};
        \addplot[blue, fill opacity=0.3] fill between[of=A and B,soft clip={domain=0:1}];
        \fill[blue,fill opacity=0.5] (axis cs:1,0) -- (axis cs:1.15,0) -- (axis cs:1.15,1) -- (axis cs:1,1) -- cycle;

        \draw[dotted] (axis cs: 1,-0.2) -- (axis cs: 1,2.5);
        \draw[dotted] (axis cs: 1.15,-0.2) -- (axis cs: 1.15,1.323);
        \node at (axis cs: 1.3,2.2){$f(x)$};
        \node at (axis cs: 1,-0.4){$x_0$};
        \node at (axis cs: 1.075,-0.1){\footnotesize{$\Delta x$}};
        \node at (axis cs: 0.8,0.3){$A(x_0)$};
        \node at (axis cs: 1.075,0.5){\footnotesize{$\Delta A$}};

        \node[rotate=90] at (axis cs: 1.3,0.5){$\underbrace{\phantom{XXXXX}}_{\rotatebox{270}{$f(x_0)$}}$};

        \end{axis}
    \end{tikzpicture}
\end{center}

Die Flächenänderung $\Delta A$, die durch das $\Delta x$ hervorgerufen wird, lässt sich durch ein Rechteck approximieren.
Diese Annäherung wird exakt mit $\lim_{\Delta x \rightarrow 0}$. Die Fläche des Rechtecks, also der Betrag der Flächenänderung,
entspricht $\Delta A = f(x_0) \cdot \Delta x$. Eingesetzt in den obenstehenden Quotienten erhält man die Gleichung
$$ A'(x)=\frac{\Delta A}{\Delta x} = \frac{f(x_0) \cdot \cancel{\Delta x}}{\cancel{\Delta x}} = f(x_0)$$
Damit lässt sich also zeigen, dass die Ableitung der Flächeninhaltsfunktion der Randfunktion entspricht.
Das bedeutet, dass die Flächenfunktion $A(x)$ die gleiche Ableitung (nämlich $f(x)$) besitzt wie per definitionem die Stammfunktion $F(x)$.
Somit unterscheidet sich die Flächenfunktion nur durch eine Konstante $C$ von der Stammfunktion.
$$ A'(x)=F'(x)=f(x)  \implies A(x) = F(x) + C $$
 $\blacksquare$.

\end{document}
