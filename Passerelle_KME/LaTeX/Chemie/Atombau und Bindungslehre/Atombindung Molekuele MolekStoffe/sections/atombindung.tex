\section{Atombindung}
\subsection{Grundbegriffe}
Ein \emph{Molekül} ist ein Teilchen aus mindestens zwei aneinander \emph{gebundenen Atomen}. Moleküle sind \emph{energieärmer} als die Einzelatome.\\
Stoffe die bei Normalbedingungen aus Molekülen bestehen, nennt man molekulare Stoffe. Zu ihnen gehören die Nichtmetalle (ohne Edelgase) und Verbindungen, die aus Nichtmetallen bestehen.\\
\emph{Zwischenmolekulare Kräfte} wirken zwischen den Molekülen und halten Feststoffe und Flüssigkeiten zusammen. Sie \emph{bestimmen} die \emph{physikalischen Eigenschaften} molekularer Stoffe. Da zwischenmolekulare Kräfte \emph{schwächer} als Atombindungen sind, kann man Moleküle trennen, ohne die Atome zu spalten.\\

\subsection{Bindungselektronen}
Atome eines Moleküls werden durch \emph{elektrostatische Kräfte} aneinander gebunden. Zwar hat ein komplettes Atom keine eigene Ladung, es besteht aber aus \emph{Elementarteilchen} die sich gegenseitig \emph{anziehen} oder abstossen.\par
\begin{figure}[h]
    \centering
        \begin{tikzpicture}
            \coordinate (origin1) at (-1.5,0);
            \coordinate (origin2) at (1.5,0);

            \draw (origin1) circle [radius = 1cm];
            \node[circle, draw = black, inner sep=2pt] (proton1) at (origin1) {$+$};
            \node[circle, fill = white, draw = black, above = 0.85cm of origin1, inner sep=0pt, minimum size = 0.1cm] (electron1) {\scriptsize $-$};
            \draw (origin2) circle [radius = 1cm];
            \node[circle, draw = black, inner sep=2pt] (proton2) at (origin2) {$+$};
            \node[circle, fill = white, draw = black, below = 0.85cm of origin2, inner sep=0pt, minimum size = 0.1cm] (electron2) {\scriptsize $-$};
            
            
            \begin{pgfonlayer}{tl1}
                \draw[>-<, >=stealth, thick, darkred, dotted] (electron1) -- (proton2);
                \draw[>-<, >=stealth, thick, darkred, dotted] (electron2) -- (proton1);
            \end{pgfonlayer}
            \begin{pgfonlayer}{tl2}
                \draw[<->, >=stealth, thick, darkblue, dashed] (electron1) -- (electron2);
                \draw[<->, >=stealth, thick, darkblue, dashed] (proton1) -- (proton2);
            \end{pgfonlayer}
            
            \draw[<->, >=stealth, thick, darkblue, dashed] (4,0.5) -- (6,0.5);
            \path (4,0.5) -- (6,0.5) node[midway, above, darkblue] {\small Abstossung};

            \draw[>-<, >=stealth, thick, darkred, dotted] (4,-0.5) -- (6,-0.5);
            \path (4,-0.5) -- (6,-0.5) node[midway, above, darkred] {\small Anziehung};
        \end{tikzpicture}
    \caption{Kräfte zwischen zwei Wasserstoffatomen}
    \label{}
\end{figure}
Zwei gebundene Atome befinden sich auf dem Abstand voneinander, bei dem die wirkenden Anziehungs- und Abstossungskräfte gleich gross sind. Will man die Atome näher zusammen oder auseinander bewegen, muss man Energie aufwenden. Der \emph{Abstand} zwischen den \emph{Kernen}, bezeichnet man als \emph{Bindungslänge}.\\
Die Elektronen, die von den Kernen angezogen werden, heissen \emph{Bindungselektronen}.

\begin{figure}[h]
    \centering
        \begin{tikzpicture} [scale = 0.5]
            \begin{pgfonlayer} {bg}
                \draw[->, > = stealth, thick] (0,0) -- (0,8);
                \draw[dashed] (0,1) -- (12,1);
                \draw[dashed] (0,7) -- (12,7);
            \end{pgfonlayer}

            \begin{pgfonlayer}{tl1}
                \shade[ball color = lightblue] (4,7) circle (.6cm);
                \shade[ball color = lightblue] (8,7) circle (.6cm);
    
                \shade[ball color = lightblue] (6,1.5) arc(70:290:0.6cm) -- cycle;
                \shade[ball color = lightblue] (6,0.37) arc(-110:110:0.6cm) -- cycle;
            \end{pgfonlayer}

            \draw[<->, > = stealth, darkblue] (1,1) -- (1,7);
            \path (1,1) -- (1,7) node[midway, below, sloped, darkblue] {\small Bindungsenergie};

            \draw[->, > = stealth, darkblue] (11,1) -- (11,7);
            \path (11,1) -- (11,7) node[midway, sloped, fill=white] {\small endotherm};
            \draw[<-, > = stealth, darkblue] (10,1) -- (10,7);
            \path (10,1) -- (10,7) node[midway, sloped, fill=white] {\small exotherm};

            \begin{pgfonlayer}{tl3}
                \node[label={\scriptsize Energie}] at (0,8) {};
                \node[label={0:{\scriptsize Energie der Einzelatome}}] at (12,7) {};
                \node[label={0:{\scriptsize Energie des Moleküls}}] at (12,1) {};
            \end{pgfonlayer}
        \end{tikzpicture}
    \caption{Bindungsenergie}
    \label{}
\end{figure}
Die \emph{Bindungsenergie} bezeichnet den Energiebetrag, der bei der Bildung eines Moleküls frei wird, oder bei der Spaltung benötigt wird.

\newpage

\subsection{Elektronenwolke und Elektronenpaare}
Die \emph{Elektronenwolke} beschreibt den Raumbereich, in dem sich ein Elektron oder Elektronenpaar mit \emph{hoher Wahrscheinlichkeit aufhält}.\\
Einfach besetzte Wolken enthalten \emph{ein ungepaartes Elektron}, doppelt besetzte Wolken enthalten \emph{zwei gepaarte Elektronen} (Elektronenpaar).\\
Jede Elektronenwolke wird \emph{zuerst einfach besetzt}. Erst wenn alle Elektronenwolken der Schale einfach besetzt sind, werden die Wolken doppelt besetzt. Da jedes Atom maximal acht Valenzelektronen besitzt, sind diese über vier Wolken verteilt.

\subsection{Die gemeinsame Elektronenwolke}
Die Atome der Moleküle sind durch \emph{gemeinsame Elektronenpaare gebunden}. Eine solche Bindung nennt sich: Atombindung, Elektronenpaarbindung oder kovalente Bindung.\\
Ein gebundenes Elektronenpaar hält sich in der gemeinsamen Elektronenwolke bevorzugt \emph{zwischen} den \emph{Kernen} auf. Bei der Bildung einer Atombindung \emph{vereinigen} sich zwei einfach besetzte Elektronenwolken, zu einer doppelt besetzten Wolke. Diese Wolke wird von beiden Atomen geteilt.

\subsection{Lewis-Formel}
Die Lewis-Formel visualisiert die \emph{Valenzelektronen} eines Atoms/Moleküls. Ein einsames Elektron wird als Punkt dargestellt, zwei gebundene Elektronen als Strich. Das Elementsymbol steht also nicht für das komplette Atom, sondern nur für den Atomrumpf.
\begin{center}
    \chemfig{\charge{0=\.}{H} \hspace{2cm} \charge{0=\., 90=\., 180=\|, 270=\|}{O} \hspace{2cm} \charge{0=\|, 90=\|, 180=\|, 270=\|}{Ar}}
\end{center}
Gemeinsame Elektronenpaare werden als Striche zwischen den Atomen dargestellt. 
\begin{center}
    \chemfig[atom sep = 0.8cm]{H-\charge{90=\|, 270=\|}{O}-H}
\end{center}
Will man die Herkunft der einzelnen Elektronen visualisieren, werden die Atome und ihre Valenzelektronen farblich markiert.\\
Die Lewis Formel stellt die \emph{Bindungsverhältnisse} zwischen den Atomen dar, gibt aber \underline{keinen} Rückschluss auf deren räumliche Struktur.

\subsection{Einfach- und Mehrfachbindungen}
Bindungen bezeichnen \emph{Kräfte zwischen Teilchen}\footnote{Nicht zu verwechseln mit Verbindungen. Verbindungen sind Reinstoffe, die sich in Elemente zersetzen lassen.}
\subsubsection*{Einfachbindung}
Sind Atome durch \emph{ein} gemeinsames Elektronenpaar gebunden, nennt man das eine Einfachbindung. 
\begin{center}
    \schemestart
    \chemfig{\charge{0=\.}{H} \qquad \charge{180=\.}{H}}
    \arrow
    \chemfig{H-H}
    \schemestop
\end{center}
Halogene\footnote{7. Hauptgruppe} besitzen drei Elektronenpaare und ein einsames Elektron. Halogene gehen oft mit sich selbst Bindungen ein (Bsp: \ch{F2}, \ch{Cl2}). Wasserstoffchlorid (\ch{HCl}) ist ein Molekül aus Wasserstoff (\ch{H}) und Chlor (\ch{Cl}). Die einsamen Valenzelektronen der beiden Atome bildet eine Einfachbindung. Das selbe gilt für Wasserstofffluorid (\ch{HF}), -bromid (\ch{HBr}) und -iodid (\ch{HI}).

\newpage

\subsubsection*{Oktettregel und Bindungswert}
Die Oktettregel besagt, dass die \emph{Nichtmetall-Atome} der zweiten Periode erreichen in \emph{Molekülen Edelgaskonfiguration}. D.h. sie sind von \emph{acht Elektronen} umgeben. Helium-Atome besitzen und Wasserstoff-Atome erreichen, in Molekülen, ein \emph{Elektronenduplett}. Die Oktettregel gilt auch für Nichtmetalle höherer Perioden, aber bei diesen ist sie mehr eine Faustregel als ein Gesetz.\\
Der Bindungswert bezeichnet die \emph{Anzahl Elektronenpaare}, an denen sich ein Nichtmetall-Atom beteiligen muss, um das \emph{Oktett} zu erreichen. Sie entspricht also der Anzahl Valenzelektronen, die bis zum Oktett fehlen. 

\subsubsection*{Doppelbindung}
Nichtmetall-Atome mit $4-6$ Valenzelektronen haben zwei Möglichkeiten das Oktett zu erreichen:
\begin{enumerate}
    \item Die Anzahl Bindungspartner entsprechen dem Bindungswert und es geht mit jedem eine Einfachbindung ein.
    \item Das Atom geht mit mindestens einem Bindungspartner eine Mehrfachbindung ein.
\end{enumerate}
Mehrfachbindungen können Doppel- (\chemfig[atom sep = 0.7cm]{A=B}) oder Dreifachbindungen (\chemfig[atom sep = 0.7cm]{A~B}) sein. Doppelbindungen bestehen aus \emph{zwei} bindenden Elektronenpaaren, Dreifachbindungen aus \emph{drei} bindenden Elektronenpaaren.\\
Mehrfachbindungen sind \emph{kürzer} und \emph{stärker} als Einfachbindungen zwischen den gleichen Atomen.\par
\begin{table}[h]
    \centering
    \small
    \caption{Bindungslängen und Bindungsenergien der C,C-Bindung}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{4.5cm} c c c} 
        & \chemfig[atom sep = 0.7cm]{C-C} & \chemfig[atom sep = 0.7cm]{C=C} & \chemfig[atom sep = 0.7cm]{C~C}\\
        Bindungslänge (in \si{\pico\metre}) & $154$ & $134$ & $120$\\
        Bindungsenergie (im Vergleich zur Einfachbindung) & $1$ & $1.8$ & $2.4$
    \end{tabular}
\end{table}
Die bindenden Elektronenpaare einer Mehrfachbindung \emph{stossen sich ab} und ihre Elektronenwolken sind dadurch gekrümmt.\\
Eine Mehrfachbindung mit mehr als drei gemeinsamen Elektronenpaaren ist nicht möglich. Eine Vierfachbindung ist geometrisch unmöglich, da sich das vierte Elektron auf der anderen Seite des Moleküls als die Dreifachbindung befindet.
\begin{center}
    \chemfig[atom sep = 0.7cm]{~\charge{0=\.}{X}}
\end{center}
Ist die Zahl der Valenzelektronen grösser als $4$ beginnen sie untereinander Elektronenpaare zu bilden.

\subsection{Polare Atombindung}
\subsubsection*{Asymmetrische Elektronenverteilung, Partialladungen}
Die gemeinsame Elektronenwolke von zwei gleichartigen Atomen ist \emph{symmetrisch}, weil \emph{beide Atomrümpfe} die Bindungselektronen \emph{gleich stark anziehen}. Solche symmetrischen Bindungen werden als \emph{unpolar}/\emph{apolar} bezeichnet. Element-Moleküle sind apolar.\\
Ziehen die Bindungspartner die gemeinsamen Elektronen \emph{unterschiedlich stark} an, ist die Elektronenwolke \emph{asymmetrisch} zwischen den beiden Zentren angeordnet. Eine polare Bindung hat eine \emph{ungleiche Ladungsverteilung}. Das Atom, welches die Elektronen stärker anzieht, erhält die \emph{Partialladung $\delta^-$} das andere die Partialladung \emph{$\delta^+$}.
\begin{center}
    \chemfig{\charge{90:4pt=$\partialpos$}{H}-\charge{90:4pt=$\partialneg$}{Cl}}
\end{center}
Zweiatomige Moleküle mit polarer Bindung haben einen positiven und negativen \glqq Pol\grqq{}, man bezeichnet sie deswegen als \emph{Dipole}. Zwischen Dipolen wirken elektrostatische Anziehungskräfte und somit ist der Dipolcharakter entscheidend für zwischenmolekulare Kräfte.

\subsubsection*{Elektronegativität}
Die Elektronegativität (EN) ist die Fähigkeit eines gebundenen Atoms, die \emph{bindenden Elektronen anzuziehen}. Die Elektronegativität ist ums grösser, je \emph{kleiner} das \emph{Atom} und je \emph{höher} seine \emph{Rumpfladung} ist.\\
\emph{Unterscheiden} sich die \emph{Bindungspartner} einer Atombindung in ihrer Elektronegativität, ist die Bindung \emph{polar}. Das Atom mit der \emph{höheren Elektronegativität} trägt die \emph{negative Partialladung}, das andere die positive. Die Bindungspolarität ist umso höher, je grösser der Elektronegativität-Unterschied ist.

\subsection{Bindungslänge, Polarität und Bindungsenergie}
\subsubsection*{Bindungslänge}
\begin{figure}[h]
    \centering
    \begin{tikzpicture}[scale = 0.8]
        \coordinate(topLeftCenter) at (-1, 2);
        \coordinate(topRightCenter) at (1, 2);
        \coordinate(bottomLeftCenter) at (-1, -2);
        \coordinate(bottomRightCenter) at (1, -2);
        \begin{pgfonlayer}{bg}
            \draw[fill = lightblue!40, draw = lightblue!40] (topLeftCenter) circle[radius = 2cm];
            \draw[fill = lightblue!40, draw = lightblue!40] (topRightCenter) circle[radius = 2cm];
            \draw[fill = lightblue!40, draw = lightblue!40] (bottomLeftCenter) circle[radius = 2cm];
            \draw[fill = lightblue!40, draw = lightblue!40] (bottomRightCenter) circle[radius = 2cm];
        \end{pgfonlayer}
        \begin{pgfonlayer}{l3}
            \draw[draw = black] (topLeftCenter) circle[radius = 1cm];
            \draw[draw = black] (topRightCenter) circle[radius = 1cm];
            \draw[draw = black] (bottomLeftCenter) circle[radius = 1cm];
            \draw[draw = black] (bottomRightCenter) circle[radius = 1cm];
        \end{pgfonlayer}
        \begin{pgfonlayer}{l2}
            \draw[fill = black] (topLeftCenter) circle[radius = 0.15cm];
            \draw[fill = black] (topRightCenter) circle[radius = 0.15cm];
            \draw[fill = black] (bottomLeftCenter) circle[radius = 0.15cm];
            \draw[fill = black] (bottomRightCenter) circle[radius = 0.15cm];
        \end{pgfonlayer}
        \draw[<->, > = stealth, draw = orange] (-0.85,2) -- (0,2);
        \draw[<->, > = stealth, draw = orange] (0.85,2) -- (0,2);
        \draw[<->, > = stealth, draw = orange] (-0.85,-2) -- (0,-2);
        \draw[<->, > = stealth, draw = orange] (0.85,-2) -- (0,-2);
        \draw[<->, > = stealth, draw = orange] (1,1.85) -- (1,0);
        \draw[<->, > = stealth, draw = orange] (1,-1.85) -- (1,0);

        \begin{pgfonlayer}{tl3}
            \path (-0.85,2) -- (0,2) node [midway, above, orange] {$r$};
            \path (0.85,2) -- (0,2) node [midway, above, orange] {$r$};
            \path (-0.85,-2) -- (0,-2) node [midway, above, orange] {$r$};
            \path (0.85,-2) -- (0,-2) node [midway, above, orange] {$r$};
            \path (1,1.85) -- (1,0) node [near end, right, orange] {$R$};
            \path (1,-1.85) -- (1,0) node [near end, right, orange] {$R$};
        \end{pgfonlayer}
    
        \begin{pgfonlayer}{tl1}
            \draw[dashed, darkblue] (1.15, 2) -- (8.5, 2);
            \draw[dashed, darkblue] (1.15, -2) -- (8.5, -2);
            \draw[dashed, darkblue] (1, -2.15) -- (1, -4.8);
            \draw[dashed, darkblue] (-1, -2.15) -- (-1, -4.8);
        \end{pgfonlayer}

        \draw[decorate, decoration = brace, thick, orange] (3.2, 2) -- (3.2, 0);
        \draw[decorate, decoration = brace, thick, orange] (3.2, 0) -- (3.2, -2);
        \draw[decorate, decoration = brace, thick, orange] (0, -4.2) -- (-1, -4.2);
        \draw[decorate, decoration = brace, thick, orange] (1, -4.2) -- (0, -4.2);
        \draw[decorate, decoration = brace, thick, darkblue] (8.5, 2) -- (8.5, -2);
        \draw[decorate, decoration = brace, thick, darkblue] (1, -4.8) -- (-1, -4.8);

        \begin{pgfonlayer}{tl3}
            \path (3.2, 2) -- (3.2, 0) node[midway, right, xshift = 1mm] {\small Van-der-Waals-Radius ($R$)};
            \path (3.2, -2) -- (3.2, 0) node[midway, right, xshift = 1mm] {\small Van-der-Waals-Radius ($R$)};
            \path (8.5, 2) -- (8.5, -2) node[midway, right, xshift = 1mm, darkblue, align=left] {\small Minimaler\\Molekülabstand};
            \path (0, -4.2) -- (-1, -4.2) node[midway, below, yshift = -1mm] {\small $r$};
            \path (0, -4.2) -- (1, -4.2) node[midway, below, yshift = -1mm](rightr) {\small $r$};
            \node[right = 0.5cm of rightr] {\small $=$ kovalenter Radius};
            \path(1, -4.8) -- (-1, -4.8) node[midway, below, yshift = -1mm, darkblue] {\small Bindungslänge};
        \end{pgfonlayer}
    \end{tikzpicture}
    \caption{Atomradien und Bindungslänge}
    \label{}
\end{figure}
Die \emph{Bindungslänge} einer Atombindung ist der \emph{Abstand} zwischen den \emph{Zentren} der \emph{gebundenen Atome}. Sie ist die Summe der kovalenten Radien. Der \emph{kovalente Radius} ist der \emph{halbe Abstand} zwischen den \emph{Zentren} einfach gebundener, gleichartiger Atome.\\
Der \emph{Van-der-Waals-Radius} ist der \emph{minimale Molekülabstand} von \emph{zwei nicht gebundenen Atomen}.

\subsubsection*{Bindungsenergie}
Die Bindungsenergie ist die Energie, die bei der \emph{Bildung} einer Atombindung \emph{frei} wird, bzw. für deren Spaltung aufgewendet werden muss. Die Einheit ist \si{\kJ\per\mole}.\\
Die Bindungsenergie einer Atombindung ist umso höher, je \emph{grösser} die \emph{Zahl} der \emph{gemeinsamen Elektronenpaare}, je \emph{kürzer} und \emph{polarer} die Bindung ist.