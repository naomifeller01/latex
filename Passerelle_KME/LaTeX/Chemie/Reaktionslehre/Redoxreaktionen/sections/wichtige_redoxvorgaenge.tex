\section{Wichtige Redoxvorgänge}
\subsection{Verbrennung und langsame Oxidation}
Eine Verbrennung ist eine rasch ablaufende Redoxreaktion, bei der meist Flammen, Glut und Rauch auftreten. Sie beginnt mit der Entzündung beim Erreichen der Entzündungstemperatur oder mit der Entflammung schon bei der Flammentemperatur. Das Oxidationsmittel ist meist Sauerstoff, jedoch nicht immer.

\begin{tabular}{p{2cm} l}
	\textbf{Glut} & Feste oder flüssige Stoffe, die Licht abgeben \\
	\textbf{Rauch} & Heterogenes Gemisch aus Gasen und Feinstoffteilchen \\
	\textbf{Flammen} & Brennende Gase
\end{tabular}

Als langsame Oxidation bezeichnen wir Reaktionen mit Sauerstoff, die ohne Lichtentstehung verlaufen. Wichtige Beispiele sind neben der Korrosion von Metallen, Oxidationsvorgänge in lebenden Zellen. Sie liefern den Lebewesen die erforderliche Betriebsenergie.

\subsection{Die Verbrennung molekularer Verbindungen}
Die Brennstoffe technisch wichtiger Verbrennungsvorgänge sind fast ausschliesslich molekulare Verbindungen und Nichtmetalle. Sie werden mit Luftsauerstoff zu Nichtmetalloxiden oxidiert.

\begin{center}
	\begin{tikzpicture}[every node/.style={text centered}]
		\node(Koef1) at (1,1) {2};
		\node[below right = -0.5cm and -0.2cm of Koef1] (H1) {\ch{H2}};
		\node[above left = -0.1cm and -0.45cm of H1] (OxH1) {{\scriptsize $\rom{1}$}}; 
		\node[below right = -0.56cm and -0.2cm of H1] (S1) {S};
		\node[above = -0.12cm of S1] (OxS1) {{\scriptsize $-\rom{2}$}};

		\node[right = 1.5cm of Koef1] (Koef2) {3};
		\node[below right = -0.5cm and -0.2cm of Koef2] (O1) {\ch{O2}};
		\node[above = -0.1cm of O1] (OcO1) {{\scriptsize $0$}};

		\node[right = 2cm of Koef2] (Koef3) {2};
		\node[below right = -0.5cm and -0.2cm of Koef3] (H2) {\ch{H2}};
		\node[above = -0.1cm of H2] (OxH2) {{\scriptsize $\rom{1}$}}; 
		\node[below right = -0.57cm and -0.2cm of H2] (O2) {\ch{O}};
		\node[above = -0.13cm of O2] (OcO2) {{\scriptsize $-\rom{2}$}};

		\node[right = 2cm of Koef3] (Koef4) {2};
		\node[right = -0.1cm of Koef4] (S2) {\ch{S}};
		\node[above = -0.1cm of S2] (OxS2) {{\scriptsize $\rom{4}$}}; 
		\node[below right = -0.5cm and -0.1cm of S2] (O3) {\ch{O2}};
		\node[above = -0.13cm of O3] (OcO3) {{\scriptsize $-\rom{2}$}};

		\node (plus1) at ($(S1)!0.5!(Koef2)$) {$+$};
		\node (plus2) at ($(O2)!0.5!(Koef4)$) {$+$};
		\draw[->, >=stealth] (O1) -- (Koef3);

		\coordinate(inBetween) at ($(O1)!0.5!(Koef3)$);
		\node[above = 0.5cm of inBetween] {Oxidation};
		\node[below = 0.5cm of inBetween] {Reduktion};

		\begin{pgfonlayer}{bg}
			\fill [black!10] (1.55,1.5) rectangle (8.3,2);
			\fill[black!10] (1.55,0.8) rectangle (2, 2);
			\fill[black!10] (7.9,0.8) rectangle (8.3, 2);

			\fill[blue!10] (3.1, 0) rectangle (8.85, 0.5);
			\fill[blue!10] (3.05, 1.5)rectangle (3.5, 0);
			\fill[blue!10] (5.9, 1.5)rectangle (6.35, 0);
			\fill[blue!10] (8.3, 1.5)rectangle (8.85, 0);
		\end{pgfonlayer}


	\end{tikzpicture}
\end{center}
Bei der Verbrennung von Schwefelwasserstoff (\ch{H2S}) entstehen Oxide der beiden Elemente. Als Oxidationsmittel dient Sauerstoff. Er oxidiert den gebundenen Schwefel von $-\rom{2}$ auf $\rom{4}$ und wird dabei von $0$ auf $-\rom{2}$ reduziert.

\newpage

\subsubsection*{Herleitung der Koeffizienten bei Verbrennungen}
Die Herleitung der Koeffizienten wird hier anhand der der Verbrennung von Butan (\ch{C4H10}) demonstriert.
Bei der Verbrennung von \ch{C4H10} entstehen \ch{CO2} und \ch{H2O}.\\
1. Aufstellen einer \textbf{provisorischen Reaktionsgleichung}.
\begin{center}
	\begin{tikzpicture}
		\node(C1) at (1,1) {\ch{C4}};
		\node[right = -0.2cm of C1] (H1) {\ch{H10}};
		\node[right = 1.2cm of C1] (O1) {\ch{O2}};
		\node[above right = -0.5cm and 3cm of C1] (C2) {\ch{C}};
		\node[right = 3.28cm of C1] (O2) {\ch{O2}};
		\node[right = 4.6cm of C1] (H2) {\ch{H2}};
		\node[above right = -0.5cm and 5cm of C1] (O3) {\ch{O}};

		\node (plus1) at ($(H1)!0.5!(O1)$) {$+$};
		\node (plus2) at ($(O2)!0.5!(H2)$) {$+$};
		\draw[->, >=stealth] (3.2, 1.05) -- (4.3, 1.05);
	\end{tikzpicture}
\end{center}

2. Herleitung der \textbf{Koeffizienten der Produkte}.
\begin{center}
	\begin{tikzpicture}
		\node(C1) at (1,1) {\ch{C4}};
		\node[right = -0.2cm of C1] (H1) {\ch{H10}};
		\node[right = 1.2cm of C1] (O1) {\ch{O2}};
		\node[above right = -0.5cm and 3cm of C1, darkblue] (Koef3) {$4$}; 
		\node[above right = -0.5cm and 3.2cm of C1] (C2) {\ch{C}};
		\node[right = 3.48cm of C1] (O2) {\ch{O2}};
		\node[above right = -0.48cm and 4.8cm of C1, darkblue] (Koef4) {$5$}; 
		\node[right = 5cm of C1] (H2) {\ch{H2}};
		\node[above right = -0.5cm and 5.4cm of C1] (O3) {\ch{O}};

		\node (plus1) at ($(H1)!0.5!(Koef2)$) {$+$};
		\node (plus2) at ($(O2)!0.5!(Koef4)$) {$+$};
		\draw[->, >=stealth] (3.2, 1.05) -- (4.3, 1.05);

		\draw[->, >=stealth, dashed, darkblue] (C1) to[out=320, in=230] (Koef3);
		\draw[->, >=stealth, dashed, darkblue] (H1) to[out=20, in=130] (Koef4);
	\end{tikzpicture}
\end{center}

3. Ermittlung der \textbf{benötigten O-Atome}. \\
Dabei wird ersichtlich, dass in diesem Fall $13$ O-Atome bei den Produkten stehen. Da Sauerstoff elementar nur als \ch{O2} auftritt, müssen wir $6\frac{1}{2}$ \ch{O2}-Moleküle aufwenden.
\begin{center}
	\begin{tikzpicture}
		\node(C1) at (1,1) {\ch{C4}};
		\node[right = -0.2cm of C1] (H1) {\ch{H10}};
		\node[right = 1.2cm of C1, darkblue] (Koef2) {$6\frac{1}{2}$};
		\node[right = 1.6cm of C1] (O1) {\ch{O2}};
		\node[above right = -0.5cm and 3.4cm of C1] (Koef3) {$4$}; 
		\node[above right = -0.5cm and 3.6cm of C1] (C2) {\ch{C}};
		\node[right = 3.88cm of C1] (O2) {\ch{O2}};
		\node[above right = -0.48cm and 5.2cm of C1] (Koef4) {$5$}; 
		\node[right = 5.4cm of C1] (H2) {\ch{H2}};
		\node[above right = -0.5cm and 5.8cm of C1] (O3) {\ch{O}};

		\node (plus1) at ($(H1)!0.5!(Koef2)$) {$+$};
		\node (plus2) at ($(O2)!0.5!(Koef4)$) {$+$};
		\draw[->, >=stealth] (3.6, 1.05) -- (4.7, 1.05);

		\node[above = 0cm of O1, darkblue] (oSum1) {{\small $13 \ch{O}$}};
		\node[above = 0cm of O2, darkblue] (oSum2) {{\small $8 \ch{O}$}};
		\node[above = 0cm of O3, darkblue] (oSum3) {{\small $5 \ch{O}$}};
	\end{tikzpicture}
\end{center}

4. Aufrunden auf ganzzahlige Koeffizienten (falls nötig).
\begin{center}
	\begin{tikzpicture}
		\node(C1) at (1,1) {\ch{C4}};
		\node[above left = -0.48cm and -0.2cm of C1] (Koef1) {$2$};
		\node[right = -0.2cm of C1] (H1) {\ch{H10}};
		\node[above right = -0.48cm and 1.2cm of C1] (Koef2) {$13$};
		\node[right = 1.6cm of C1] (O1) {\ch{O2}};
		\node[above right = -0.5cm and 3.4cm of C1] (Koef3) {$8$}; 
		\node[above right = -0.5cm and 3.6cm of C1] (C2) {\ch{C}};
		\node[right = 3.88cm of C1] (O2) {\ch{O2}};
		\node[above right = -0.48cm and 5.2cm of C1] (Koef4) {$10$}; 
		\node[right = 5.6cm of C1] (H2) {\ch{H2}};
		\node[above right = -0.5cm and 6cm of C1] (O3) {\ch{O}};

		\node (plus1) at ($(H1)!0.5!(Koef2)$) {$+$};
		\node (plus2) at ($(O2)!0.5!(Koef4)$) {$+$};
		\draw[->, >=stealth] (3.6, 1.05) -- (4.7, 1.05);
	\end{tikzpicture}
\end{center}

\subsection{Die Verbrennung von Kohlenwasserstoff ist exotherm}
Bei der Verbrennung molekularer Verbindungen entstehen in der Regel Oxide aller in der Verbindung gebundenen Elemente. Die Brennstoffe, die aus dem fossilen Energieträgern\footnote{Fossile Brennstoffe: Kohle, Erdöl und Erdgas} gewonnen werden, liefern über $80\%$ der technisch genutzten Energie. Sie bestehen, mit Ausnahme von Kohle, hauptsächlich aus Kohlenwasserstoffen. Kohle selber enthält zusätzlich auch elementaren Kohlenstoff. Bei ihrer Verbrennung entstehen Kohlenstoffdioxid und Wasserdampf. Weil diese deutlich energieärmer sind als die Kohlenwasserstoffe, verlaufen die Reaktionen stark exotherm ab. Zur Spaltung der schwachen polaren (\ch{C\bond{sb}H}) und der unpolaren (\ch{O\bond{sb}O}, \ch{C\bond{sb}C}) Bindungen der Edukte ist weniger Energie nötig, als bei der Bildung der stark polaren Bindungen (\ch{C\bond{db}O}, \ch{O\bond{sb}H}) der Produkte frei wird.

\subsection{Die Oxidation von Metallen}
Die meisten Metalle oxidieren an der Luft. Sie laufen an und verlieren ihren metallischen Glanz. Viele korrodieren durch die Reaktion mit Sauerstoff und anderen Stoffen aus der Umgebung (Wasser, Säuren, Salze). Korrosionsvorgänge wie das Rosten können metallische Bauteile beeinträchtigen. Mit Legierungen, Schutzüberzügen und Schutzanstriche versucht man Metalle vor der Korrosion zu schützen.

\subsection{Exkurs: Gewinnung von Metallen durch Reduktion}
Die meisten Metalle kommen in der Erdkruste nicht elementar vor und werden darum durch Reduktion aus ihren Verbindungen gewonnen. Als Reduktionsmittel dienen dabei unter anderem Kohlenstoff oder unedle Metalle. \\
Eisen wird im Hochofen mithilfe von Steinkohlekoks aus Eisenoxiden gewonnen. Reduktionsmittel sind dabei Kohlenstoff und Kohlenstoffmonooxid.
